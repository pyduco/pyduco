# pyDuCo
_from PYthon DUblin COre \
pronunciation:_ \
π + 🦆 + O
---

>"Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative with global recognition. See https://www.dublincore.org/about/copyright or  `LICENSE.txt` for details.


## System Requirements


Supported operating systems:
- Unix
- Windows

The minimum python version required for `pyDuCo` is 3.10.
Supported the python versions:

- 3.10 - 3.12

`pyDuCo` depends on the following python packages, which will be automatically installed during the setup:
- `pytest`
- `pyyaml`
- `versioneer`

## Installation

Cloning the repository from gitlab:

```shell
git clone https://git.rwth-aachen.de/pyduco/pyduco.git
```

Adding the package to your virtual environment using pip:

```shell
cd pyDuCo
pip install .
```


## Verify the installation (execute the tests)

Testing uses the `unittest` module and the `pytest`package. Navigate a terminal to the main directory of your clone, 
if not done already in the last step. It is important to execute the tests from the `pyDuCo` root directory, which is 
most likely called `pyDuCo` and contains the package directory `dublincore`. Activate your python environment. 
Execute the tests with:

```shell
pytest tests
```

---

# Usage

Import the package

`import dublincore as dc`

Create a metadata object which accompanies your class object, simulation directory or
```python
myMetaData = dc.MetaData()
myMetaData = dc.yaml2metadata("~/projects/myAwesomeProject/metadata.yaml")
```

Print the docstrings, describing the usage of specific `pyDuCo` functions
```python
help(dc.MetaData)
help(dc.obj2metadata)
```

Directly store some metadata in the MetaData object.

```python
myMetaData["abstract"] = "This is my project, finding an important result."
myMetaData.date = datetime.datetime.now()
myMetaData.update(otherMetadata)
```

Take advantage of automized metadata collection from directories or python objects
```python
myMetaData = dc.obj2metadata(myRunnerObject)
myMetaData = dc.obj2metadata(myCanteraSolutionObject)
```

Complement your previously stored metadata from your environment
````python
myMetaData.stock_up(dc.obj2metadata(os.environ))
````

Write the metadata next to your data to document your work. Supported formats:
- `yaml` (for machine readability)
- `markdown` (human readable, `yaml` header can be included)
- As Obsidian vault (For linked, nested metadata, see https://obsidian.md/)

```python
metadata2yaml(myMetaData, "~/Simulations/myAwesomeSimulationCase/metadata.yaml")
metadata2markdown(myMetaData, "~/Simulations/myAwesomeSimulationCase/README.md", "--include-yaml")
metadata2obsidian(myMetaData, "~/Simulations/myAwesomeSimulationCase")
```

See the examples for further documentation.

---

# License

See LICENSE.txt

# Acknowledgement

This software is being developed within the project [NFDI4Ing](https://nfdi4ing.de).

Funded by the [German Research Foundation (DFG)](https://www.dfg.de/en/) - project number [442146713](https://gepris.dfg.de/gepris/projekt/442146713?context=projekt&task=showDetail&id=442146713&).

---

# The pyDuCo duck-type mascot

```
   \ \__/ /    *------*
   ( o  o )   / quack |
   / .  . \  /_-------'
  (________)
   _:    :_..**##**..__
 ."                    """>
#                        #    
#                      ,'
 `*~~~~~~~~~~~~~~~~~~."    It does quack, hence a duck!
```
