# demo_metadata

## Date

|      |            |
|------|------------|
| date | 11.11.1111 |

## Overview

|            |                            |
|------------|----------------------------|
| identifier | demo_metadata              |
| language   | code: C++<br>text: english |

## Relations

|                 |               |
|-----------------|---------------|
| Is a version of | original_data |

## Parties

|             |                              |
|-------------|------------------------------|
| creator     | Not Me                       |
| contributor | Contributor1<br>Contributor2 |
