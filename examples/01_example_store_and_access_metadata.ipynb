{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Storing and accessing metadata with a MetaData object\n",
    "\n",
    "A MetaData object can be used to store metadata of any type. In this example, we will manually add some metadata to a MetaData object to explore how it can be accessed and handled.\n",
    "\n",
    "We start with creating a MetaData object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import dublincore as dc\n",
    "\n",
    "my_meta_data = dc.MetaData()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## MetaData - dict analogy\n",
    "\n",
    "The MetaData object has most functionalities of a `dict`. In fact, internally all the metadata is stored inside a dict. This means, that it can handle key-value pairs \"as known\":"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  some_key: 5\n",
      " other_key: has not changed\n",
      "   new_key: []\n",
      "Number of metadata entries: 3\n"
     ]
    }
   ],
   "source": [
    "my_meta_data[\"some_key\"] = 5\n",
    "my_meta_data[\"other_key\"] = \"has not changed\"\n",
    "\n",
    "my_meta_data.update({\"new_key\": []}) # This would overwrite any existing data if the term is already filled.\n",
    "\n",
    "# print all stored key-value pairs\n",
    "for key in my_meta_data:\n",
    "    print(\"{0:>10}: {1}\".format(key, my_meta_data[key]))\n",
    "\n",
    "# access the length of the MetaData\n",
    "print(\"Number of metadata entries:\", len(my_meta_data))\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are further member functions to manipulate the metadata in the MetaData object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "  some_key: [5, 'any_value']\n",
      " other_key: has not changed\n",
      "   new_key: []\n",
      "     key 4: ['value', 4, 5]\n",
      "other_key2: has changed\n"
     ]
    }
   ],
   "source": [
    "# add a single key-value pair\n",
    "my_meta_data.add_term(\"key 4\", [\"value\", 4])\n",
    "\n",
    "# add an item to a key-value pair. The value will become a list, if not already.\n",
    "my_meta_data.add_to_term(\"some_key\", \"any_value\")\n",
    "my_meta_data.add_to_term(\"key 4\", 5)\n",
    "\n",
    "# use the .stock_up() function equivalent to .update(), but without overwriting any data\n",
    "my_meta_data.stock_up({\"other_key\": \"has_changed\", \"other_key2\": \"has changed\"})\n",
    "\n",
    "# print all stored key-value pairs\n",
    "for key in my_meta_data:\n",
    "    print(\"{0:>10}: {1}\".format(key, my_meta_data[key]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['some_key', 'other_key', 'new_key', 'key 4', 'other_key2']"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_meta_data.get_terms()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Dublin Core™ metadata terms\n",
    "\n",
    "The [Dublin Core™ metadata specification](https://www.dublincore.org/) specifies a list of 53 terms, in which metadata shall be preferably stored. This standardizes the metadata and makes it machine-searchable. 16 out of the 53 terms are mandatory to properly describe a dataset. The complete list of terms from the Dublin Core™ specification and the list of mandatory terms is stored in the MetaData class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "All Dublin Core™ terms:\n",
      "\n",
      "['abstract', 'accessRights', 'accrualMethod', 'accrualPeriodicity', 'accrualPolicy', 'alternative', 'audience', 'available', 'bibliographicCitation', 'conformsTo', 'contributor', 'coverage', 'created', 'creator', 'date', 'accepted', 'copyrighted', 'submitted', 'description', 'extent', 'format', 'hasFormat', 'hasPart', 'hasVersion', 'identifier', 'instructionalMethod', 'isFormatOf', 'isPartOf', 'isReferencedBy', 'isReplacedBy', 'isRequiredBy', 'issued', 'isVersionOf', 'language', 'license', 'mediator', 'medium', 'modified', 'provenance', 'publisher', 'references', 'relation', 'replaces', 'requires', 'rights', 'rightsHolder', 'source', 'spatial', 'subject', 'temporal', 'title', 'type', 'valid']\n",
      "\n",
      "Mandatory Dublin Core™ terms:\n",
      "\n",
      "['contributor', 'coverage', 'creator', 'date', 'description', 'format', 'identifier', 'language', 'publisher', 'relation', 'rights', 'source', 'subject', 'title', 'type', 'valid']\n",
      "\n",
      "Still missing:\n",
      "\n",
      "['contributor', 'coverage', 'creator', 'date', 'description', 'format', 'identifier', 'language', 'publisher', 'rights', 'source', 'subject', 'title', 'type', 'valid']\n"
     ]
    }
   ],
   "source": [
    "# print all terms \n",
    "print(\"All Dublin Core™ terms:\\n\")\n",
    "print(dc.MetaData.TERMS_DUBLIN_CORE_ALL)\n",
    "\n",
    "# print the mandatory terms\n",
    "print(\"\\nMandatory Dublin Core™ terms:\\n\")\n",
    "print(dc.MetaData.OBLIGATORY_TERMS_DUBLIN_CORE)\n",
    "\n",
    "#print the mandatory terms which are not filled yet\n",
    "print(\"\\nStill missing:\\n\")\n",
    "print(my_meta_data.get_unfilled_terms())\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The terms which follow the official specification are available as properties in the MetaData object. They will also be found by modern IDEs and proposed by their autocompletion. This shall help to store metadata in a structured and standardized manner. All the terms have also descriptions associated, which stem from the official [publication of DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/). They can be accessed using the help function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on property:\n",
      "\n",
      "    An unambiguous reference to the resource within a given context.\n",
      "    ---\n",
      "    Recommended practice is to identify the resource by means of a string conforming to an identification system.\n",
      "    Examples include International Standard Book Number (ISBN), Digital Object Identifier (DOI), and Uniform\n",
      "    Resource Name (URN). Persistent identifiers should be provided as HTTP URIs.\n",
      "\n"
     ]
    }
   ],
   "source": [
    "import datetime\n",
    "my_meta_data.date = datetime.datetime.now()\n",
    "my_meta_data.creator = \"Prof. Dr. Clever\"\n",
    "\n",
    "# print the official description for the term \"identifier\"\n",
    "help(dc.MetaData.identifier)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If initializing a MetaData object with a single positional argument, this will be used as the identifier.\n",
    "The identifier will also be returned if the MetaData object is transformed to a string."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The second MetaData object\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "'The second MetaData object'"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "my_meta_data2 = dc.MetaData(\"The second MetaData object\")\n",
    "print(my_meta_data2.identifier)\n",
    "str(my_meta_data2)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "venv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
