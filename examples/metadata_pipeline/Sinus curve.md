# Sinus curve

## Overview

|            |             |  |
|------------|-------------|--|
| identifier | Sinus curve |  |
| extent     | (0.0, 8.0)  |  |
| type       | dataset     |  |

## Referenced by

|                  |  |
|------------------|--|
| [[example_plot]] |  |

## Accretion

|               |               |
|---------------|---------------|
| accrualMethod | Applied Sinus |
