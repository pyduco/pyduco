# copy of Sinus curve

## Overview

|            |                     |  |
|------------|---------------------|--|
| identifier | copy of Sinus curve |  |
| extent     | (0.0, 8.0)          |  |
| type       | dataset             |  |

## Relations

|        |                 |
|--------|-----------------|
| Source | [[Sinus curve]] |

## Referenced by

|                  |  |
|------------------|--|
| [[example_plot]] |  |

## Accretion

|               |                                   |
|---------------|-----------------------------------|
| accrualMethod | Applied Sinus<br>Stretched by 2.0 |
