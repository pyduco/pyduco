# example_plot

## Overview

|            |              |
|------------|--------------|
| identifier | example_plot |

## Relations

|            |                                            |
|------------|--------------------------------------------|
| References | [[Sinus curve]]<br>[[copy of Sinus curve]] |
