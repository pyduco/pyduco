
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import datetime
import os
import unittest
from dublincore.metadata2obj import *
from dublincore.metadata import MetaData


def create_meta_data() -> MetaData:
    meta_data = MetaData()
    meta_data["mapped_key_one"] = None
    meta_data["mapped_key_2"] = ["huhu"]
    meta_data["adjusted_additional_key"] = 56.123
    return meta_data


class TestObj2MetaData(unittest.TestCase):
    def test_obj2metadata(self):
        # collecting metadata from this crawler is a bit of a casino and not really defined. One has to hope that
        # something is found and check the data afterwards. In a first approach let's check that no errors are thrown if
        # the function is used on different objects

        self.assertIsNotNone(obj2metadata(None))
        self.assertIsNotNone(obj2metadata(3))
        self.assertIsNotNone(obj2metadata(345.756))
        self.assertIsNotNone(obj2metadata("Huhu"))
        self.assertIsNotNone(obj2metadata(["Huhu", "Haha"]))
        self.assertIsNotNone(obj2metadata([23, 89]))
        self.assertIsNotNone(obj2metadata({"Huhu": 456.5, "Haha": [4, 5, 6]}))
        self.assertIsNotNone(obj2metadata(datetime.datetime.now()))
        self.assertIsNotNone(obj2metadata(os.environ))
        self.assertIsNotNone(obj2metadata(MetaData()))

    def test_second_argument_unchanged(self):

        self.assertIsNotNone(obj2metadata(None))
        self.assertIsNotNone(obj2metadata({"Huhu": 456.5, "Haha": [4, 5, 6]}))
        self.assertIsNotNone(obj2metadata(os.environ))


    def test_metadata2metadata(self):
        meta1 = create_meta_data()
        meta2 = obj2metadata(meta1)

        for term in meta1:
            self.assertEqual(meta1[term], meta2[term])


if __name__ == '__main__':
    unittest.main()
