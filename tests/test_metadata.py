
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import unittest
from dublincore.metadata import MetaData


OBLIGATORY_TERMS_DUBLIN_CORE = ["contributor", "coverage", "creator", "date", "description", "format", "identifier",
                                "language", "publisher", "relation", "rights", "source", "subject", "title", "type",
                                "valid"]


OBLIGATORY_TERMS_STFS = ["contributor", "coverage", "creator", "date", "description", "format", "identifier",
                         "language", "publisher", "rights", "source", "subject", "title", "type",
                         "valid"]


TERMS_DUBLIN_CORE_ALL = ["abstract", "accessRights", "accrualMethod", "accrualPeriodicity", "accrualPolicy",
                         "alternative", "audience", "available", "bibliographicCitation",  "conformsTo",
                         "contributor", "coverage", "created", "creator",  "date",
                         "accepted", "copyrighted", "submitted", "description",  "extent",
                         "format", "hasFormat", "hasPart", "hasVersion",  "identifier",
                         "instructionalMethod", "isFormatOf", "isPartOf", "isReferencedBy",  "isReplacedBy",
                         "isRequiredBy", "issued", "isVersionOf", "language",  "license",
                         "mediator", "medium", "modified", "provenance",  "publisher",
                         "references", "relation", "replaces", "requires",  "rights",
                         "rightsHolder", "source", "spatial", "subject",  "temporal",
                         "title", "type", "valid"]

TERMS_DUBLIN_CORE_FUNCTIONS = ["abstract", "accessRights", "accrualMethod", "accrualPeriodicity", "accrualPolicy",
                               "alternative", "audience", "dateAvailable", "bibliographicCitation",  "conformsTo",
                               "contributor", "coverage", "dateCreated", "creator",  "date",
                               "dateAccepted", "dateCopyrighted", "dateSubmitted", "description",  "extent",
                               "format", "relationHasFormat", "relationHasPart", "relationHasVersion", "identifier",
                               "instructionalMethod", "relationIsFormatOf", "relationIsPartOf",
                               "relationIsReferencedBy",  "relationIsReplacedBy",
                               "relationIsRequiredBy", "dateIssued", "relationIsVersionOf", "language",  "license",
                               "mediator", "medium", "dateModified", "provenance",  "publisher",
                               "relationReferences", "relation", "relationReplaces", "relationRequires",  "rights",
                               "rightsHolder", "relationSource", "spatial", "subject",  "temporal",
                               "title", "type", "dateValid"]


class TestMetaData(unittest.TestCase):

    def test_constructor(self):
        my_meta = MetaData()
        self.assertEqual(my_meta.get_terms(), [])
        self.assertEqual(my_meta.obligatory_terms, MetaData.OBLIGATORY_TERMS_STFS)
        
        self.assertFalse(my_meta.is_adequately_filled())
        self.assertEqual(len(my_meta), 0)
        self.assertEqual(str(my_meta), "Unidentified metadata object")
        self.assertFalse("test" in my_meta)
        self.assertFalse("date" in my_meta)
        self.assertFalse("identifier" in my_meta)
        self.assertFalse(456 in my_meta)
        self.assertFalse(456.789e4 in my_meta)
        
        self.assertEqual(my_meta.get_unfilled_terms(), MetaData.OBLIGATORY_TERMS_STFS)
        self.assertEqual(my_meta.get_unfilled_terms(), my_meta.obligatory_terms)
        self.assertEqual([t for t in my_meta.iter_unfilled_terms()], my_meta.get_unfilled_terms())

    def test_constructor_with_name(self):
        my_meta = MetaData("12345_ITEM_BANANA")
        self.assertEqual(my_meta.get_terms(), ["identifier"])
        self.assertEqual(my_meta.obligatory_terms, MetaData.OBLIGATORY_TERMS_STFS)

        self.assertFalse(my_meta.is_adequately_filled())
        self.assertEqual(len(my_meta), 1)
        self.assertEqual(str(my_meta), "12345_ITEM_BANANA")
        self.assertFalse("test" in my_meta)
        self.assertFalse("date" in my_meta)
        self.assertTrue("identifier" in my_meta)
        self.assertFalse(456 in my_meta)
        self.assertFalse(456.789e4 in my_meta)

    def test_obligations(self):
        self.assertEqual(MetaData.OBLIGATORY_TERMS_DUBLIN_CORE, OBLIGATORY_TERMS_DUBLIN_CORE)
        self.assertEqual(MetaData.OBLIGATORY_TERMS_STFS, OBLIGATORY_TERMS_STFS)
        self.assertEqual(MetaData.TERMS_DUBLIN_CORE_ALL, TERMS_DUBLIN_CORE_ALL)

    def test_setters_available(self):
        funcs = dir(MetaData())
        for term in TERMS_DUBLIN_CORE_FUNCTIONS:
            self.assertIn(term, funcs)

    def test_access_empty_term(self):
        my_meta = MetaData()
        self.assertIsNone(my_meta.abstract)

        terms = MetaData.TERMS_DUBLIN_CORE_ALL
        for attr in dir(my_meta):
            if attr in terms:
                self.assertIsNone(getattr(my_meta, attr))

    def test_access_terms(self):
        my_meta = MetaData()
        my_meta.date = 789
        my_meta["identifier"] = "it's-a me, Mario!"
        
        obligatory_terms = MetaData.OBLIGATORY_TERMS_STFS.copy()
        obligatory_terms.remove("date")
        obligatory_terms.remove("identifier")

        self.assertFalse(my_meta.is_adequately_filled())
        self.assertEqual(len(my_meta), 2)
        self.assertEqual(str(my_meta), "it's-a me, Mario!")
        self.assertFalse("test" in my_meta)
        self.assertTrue("date" in my_meta)
        self.assertTrue("identifier" in my_meta)
        self.assertFalse(456 in my_meta)
        self.assertFalse(456.789e4 in my_meta)
        
        self.assertEqual(my_meta.date, 789)
        self.assertEqual(my_meta["date"], 789)
        self.assertEqual(my_meta["identifier"], "it's-a me, Mario!")
        self.assertEqual(my_meta.identifier, "it's-a me, Mario!")
        
        self.assertEqual(my_meta.get_terms(), ["date", "identifier"])
        self.assertEqual([t for t in my_meta], ["date", "identifier"])
        self.assertEqual(my_meta.get_unfilled_terms(), obligatory_terms)
        self.assertEqual([t for t in my_meta.iter_unfilled_terms()], obligatory_terms)

    def test_search_with_lists_in_terms(self):
        my_meta = MetaData()
        my_meta.date = 789
        my_meta["identifier"] = "it's-a me, Mario!"
        my_meta.relationHasPart = [MetaData(), MetaData()]

        self.assertTrue("date" in my_meta)
        self.assertTrue("identifier" in my_meta)
        self.assertTrue("hasPart" in my_meta)
        self.assertFalse(789 in my_meta)

    def test_update(self):
        my_meta1 = MetaData()
        my_meta1.date = 789
        my_meta1["identifier"] = "it's-a me, Mario!"

        my_meta2 = MetaData()
        my_meta2.title = "Awesome Metadata"
        my_meta2["contributor"] = "Luigi"

        obligatory_terms = MetaData.OBLIGATORY_TERMS_STFS.copy()
        obligatory_terms.remove("date")
        obligatory_terms.remove("title")
        obligatory_terms.remove("identifier")
        obligatory_terms.remove("contributor")

        self.assertTrue("identifier" in my_meta1)
        self.assertTrue("date" in my_meta1)
        self.assertFalse("title" in my_meta1)
        self.assertFalse("contributor" in my_meta1)

        self.assertFalse("identifier" in my_meta2)
        self.assertFalse("date" in my_meta2)
        self.assertTrue("title" in my_meta2)
        self.assertTrue("contributor" in my_meta2)

        my_meta = my_meta1.update(my_meta2)

        self.assertFalse(my_meta.is_adequately_filled())
        self.assertEqual(len(my_meta), 4)
        self.assertEqual(str(my_meta), "it's-a me, Mario!")

        self.assertTrue("identifier" in my_meta)
        self.assertTrue("date" in my_meta)
        self.assertTrue("title" in my_meta)
        self.assertTrue("contributor" in my_meta)

        self.assertEqual(my_meta.get_unfilled_terms(), obligatory_terms)
        self.assertEqual([t for t in my_meta.iter_unfilled_terms()], obligatory_terms)

    def test_not_attached(self):
        # This test would test whether all metadata entries which come from mutable sources are deattached from their
        # respective originals. This must, however, not be the case if the source is a MetaData itself.

        meta_data = MetaData()
        authors = ["me", "co-author 1", "co-author 2"]
        meta_data["authors"] = authors

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2"])
        self.assertEqual(authors, ["me", "co-author 1", "co-author 2"])

        meta_data["authors"].append("New Author")

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2", "New Author"])
        self.assertEqual(authors, ["me", "co-author 1", "co-author 2"]),

    def test_update_not_attached(self):
        # This is an adapted copy of the not attached test

        meta_data = MetaData()
        my_dict = {"authors": ["me", "co-author 1", "co-author 2"]}

        meta_data.update(my_dict)

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2"])
        self.assertEqual(my_dict["authors"], ["me", "co-author 1", "co-author 2"])

        meta_data["authors"].append("New Author")

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2", "New Author"])
        self.assertEqual(my_dict["authors"], ["me", "co-author 1", "co-author 2"]),

    def test_attached(self):
        # Tests, if a MetaData type metadata entry is copied by reference.
        # This is such that Relationships do not end up in infinity copy loops.

        outer_meta_data = MetaData()
        inner_meta_data = MetaData()
        my_dict = {"identifier": "Reference Setup"}

        inner_meta_data.update(my_dict)
        outer_meta_data["source"] = inner_meta_data

        self.assertEqual(outer_meta_data.relationSource, inner_meta_data)

        outer_meta_data.relationSource.identifier = "Another Reference Setup"

        self.assertEqual(outer_meta_data.relationSource, inner_meta_data)
        self.assertEqual(inner_meta_data.identifier, "Another Reference Setup")

    def test_attached_list(self):
        # Tests, if a list[MetaData] type metadata entry is copied by reference.
        # This is such that Relationships do not end up in infinity copy loops.

        outer_meta_data = MetaData()
        inner_meta_data1 = MetaData()
        inner_meta_data2 = MetaData()
        my_dict = {"identifier": "Reference Setup"}

        inner_meta_data1.update(my_dict)
        inner_meta_data2.update(my_dict)
        outer_meta_data["hasPart"] = [inner_meta_data1, inner_meta_data2]

        self.assertEqual(outer_meta_data.relationHasPart, [inner_meta_data1, inner_meta_data2])

        outer_meta_data.relationHasPart[1].identifier = "Another Reference Setup"

        self.assertEqual(outer_meta_data.relationHasPart[1], inner_meta_data2)
        self.assertEqual(inner_meta_data2.identifier, "Another Reference Setup")

    def test_attached_dict(self):
        # Tests, if a list[MetaData] type metadata entry is copied by reference.
        # This is such that Relationships do not end up in infinity copy loops.

        outer_meta_data = MetaData()
        inner_meta_data1 = MetaData()
        inner_meta_data2 = MetaData()
        my_dict = {"identifier": "Reference Setup"}

        inner_meta_data1.update(my_dict)
        inner_meta_data2.update(my_dict)
        outer_meta_data["hasPart"] = {"a": inner_meta_data1, "b": inner_meta_data2}

        self.assertEqual(outer_meta_data.relationHasPart, {"a": inner_meta_data1, "b": inner_meta_data2})

        outer_meta_data.relationHasPart["b"].identifier = "Another Reference Setup"

        self.assertEqual(outer_meta_data.relationHasPart["b"], inner_meta_data2)
        self.assertEqual(inner_meta_data2.identifier, "Another Reference Setup")


    def test_update_with_dict(self):
        my_meta1 = MetaData()
        my_meta1.date = 789
        my_meta1["identifier"] = "it's-a me, Mario!"

        dictionary = {"title": "Awesome Metadata", "contributor": "Luigi"}

        obligatory_terms = MetaData.OBLIGATORY_TERMS_STFS.copy()
        obligatory_terms.remove("date")
        obligatory_terms.remove("title")
        obligatory_terms.remove("identifier")
        obligatory_terms.remove("contributor")

        my_meta = my_meta1.update(dictionary)

        self.assertFalse(my_meta.is_adequately_filled())
        self.assertEqual(len(my_meta), 4)
        self.assertEqual(str(my_meta), "it's-a me, Mario!")

        self.assertTrue("identifier" in my_meta)
        self.assertTrue("date" in my_meta)
        self.assertTrue("title" in my_meta)
        self.assertTrue("contributor" in my_meta)

        self.assertEqual(my_meta.get_unfilled_terms(), obligatory_terms)
        self.assertEqual([t for t in my_meta.iter_unfilled_terms()], obligatory_terms)

    def test_list_terms(self):
        my_meta = MetaData()

        self.assertFalse("isPartOf" in my_meta)

        my_meta.relationIsPartOf = "parentStudy"
        self.assertTrue("isPartOf" in my_meta)
        self.assertEqual(my_meta.relationIsPartOf, ["parentStudy"])
        self.assertEqual(my_meta["isPartOf"], ["parentStudy"])

        my_meta.addRelationIsPartOf("otherParentStudy")
        self.assertEqual(my_meta.relationIsPartOf, ["parentStudy", "otherParentStudy"])

        my_meta.addRelationIsPartOf(["thirdParentStudy"])
        self.assertEqual(my_meta.relationIsPartOf, ["parentStudy", "otherParentStudy", "thirdParentStudy"])

        my_meta.relationIsPartOf = ["parentStudy", "otherParentStudy"]
        self.assertEqual(my_meta.relationIsPartOf, ["parentStudy", "otherParentStudy"])

        my_meta.addRelationIsPartOf(["thirdParentStudy", "forthParentStudy"])
        self.assertEqual(my_meta.relationIsPartOf, ["parentStudy", "otherParentStudy",
                                                    "thirdParentStudy", "forthParentStudy"])

    def test_append_empty_term(self):
        my_meta = MetaData()
        my_meta.addRelationReferences("asd")
        my_meta.addRelationIsPartOf([456])

        self.assertEqual(my_meta.relationReferences, ["asd"])
        self.assertEqual(my_meta.relationIsPartOf, [456])

    def test_equal(self):
        my_meta1 = MetaData()
        my_meta2 = MetaData()
        self.assertEqual(my_meta1, my_meta2)

        my_meta1.title = "myTitle"
        my_meta2.title = "myTitle"
        my_meta1.date = 345
        self.assertNotEqual(my_meta1, my_meta2)

        my_meta2.date = 345
        self.assertEqual(my_meta1, my_meta2)

        my_meta1.date = 123
        self.assertNotEqual(my_meta1, my_meta2)

        my_meta2.date = 123
        my_meta2.obligatory_terms = 789
        self.assertNotEqual(my_meta1, my_meta2)

    def test_stock_up_not_attached(self):
        # This is an adapted copy of the not attached test

        meta_data = MetaData()
        my_dict = {"authors": ["me", "co-author 1", "co-author 2"]}

        meta_data.stock_up(my_dict)

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2"])
        self.assertEqual(my_dict["authors"], ["me", "co-author 1", "co-author 2"])

        meta_data["authors"].append("New Author")

        self.assertEqual(meta_data["authors"], ["me", "co-author 1", "co-author 2", "New Author"])
        self.assertEqual(my_dict["authors"], ["me", "co-author 1", "co-author 2"])

    def test_stock_up(self):
        my_meta1 = MetaData()
        my_meta1.identifier = "myName1"
        my_meta1.date = "456"
        my_meta1.creator = ["me"]

        my_meta2 = MetaData()
        my_meta2.identifier = "myName2"
        my_meta2.date = 789

        my_meta2_ref = my_meta2.copy()
        my_meta2_ref["creator"] = ["me"]

        my_meta1_stocked = my_meta1.copy()
        my_meta1_backup = my_meta1.copy()
        my_meta2_stocked = my_meta2.copy()
        my_meta2_backup = my_meta2.copy()

        my_meta1_stocked.stock_up(my_meta2)
        # Nothing must have changed at the mutable argument
        self.assertEqual(my_meta2, my_meta2_backup)

        my_meta2_stocked.stock_up(my_meta1)
        # Nothing must have changed at the mutable argument
        self.assertEqual(my_meta1, my_meta1_backup)

        self.assertEqual(my_meta2_stocked, my_meta2_ref)

    def test_copy(self):
        my_meta1 = MetaData()
        my_meta1.title = "myTitle"
        my_meta1.date = 345
        my_meta2 = my_meta1.copy()

        self.assertEqual(my_meta1, my_meta2)
        self.assertIsNot(my_meta1, my_meta2)

    def test_clear_non_obligatory_terms(self):
        meta_data = MetaData()
        meta_data["identifier"] = "It's-a me: Mario"
        meta_data["date"] = "today"
        meta_data_obligatory = meta_data.copy()
        self.assertEqual(meta_data, meta_data_obligatory)
        self.assertIsNot(meta_data, meta_data_obligatory)
        meta_data["hasPart"] = ["arms", "feet", "Mario-costume"]
        meta_data_dublincore = meta_data.copy()
        self.assertEqual(meta_data, meta_data_dublincore)
        self.assertIsNot(meta_data, meta_data_dublincore)

        meta_data["unnecessary detail"] = "You do not want to know that!"
        self.assertNotEqual(meta_data, meta_data_obligatory)
        self.assertNotEqual(meta_data, meta_data_dublincore)
        meta_data.clear_non_dublin_core_terms()
        self.assertNotEqual(meta_data, meta_data_obligatory)
        self.assertEqual(meta_data, meta_data_dublincore)
        meta_data.clear_non_obligatory_terms()
        self.assertEqual(meta_data, meta_data_obligatory)
        self.assertNotEqual(meta_data, meta_data_dublincore)

    def test_mini_copy(self):
        meta_data1 = MetaData()
        meta_data1["identifier"] = "It's-a me: Mario"
        meta_data1["date"] = "today"
        meta_data1["description"] = "Super cool"
        meta_data1["something non-specified"] = "crap"

        meta_data2 = MetaData()
        meta_data2["identifier"] = "It's-a me: Mario"
        meta_data2["description"] = "Super cool"
        meta_data2.obligatory_terms = []

        mini_version = meta_data1.mini_copy()
        self.assertEqual(mini_version, meta_data2)

        mini_version.identifier = "Luigi"
        self.assertEqual(meta_data1.identifier, "It's-a me: Mario", "mini_copy is not detached from metadata. "
                                                                    "Copy before return")


if __name__ == '__main__':
    unittest.main()
