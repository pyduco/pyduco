
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import os.path
import shutil
import unittest
from dublincore.metadata import MetaData
from dublincore.metadata2markdown import metadata2markdown


def read_file(path: str) -> list[str]:
    with open(path) as file:
        lines = [line.strip("\n") for line in file.readlines()]
    # re-append the trailing newline
    lines.append("")
    return lines


def create_meta_data() -> MetaData:

    mario = MetaData()
    mario["description"] = "The coolest Italian guy with the monster mustache"
    mario["identifier"] = "It's-a me, Mario!"
    mario["isReferencedBy"] = 123456789
    mario["publisher"] = "Anonymous Japanese company"
    mario["requires"] = {"identifier": "Mario's Cart", "color": "red", "speed": 99999}
    mario["hasFormat"] = ["Nintendo", "PC", "Wi"]

    blue_mario = mario.copy()
    blue_mario["identifier"] = "Blue Mario"
    blue_mario["description"] = "A blue Italian guy with a monster mustache"
    green_mario = mario.copy()
    green_mario["identifier"] = "Luigi"
    green_mario["description"] = "A green Italian guy with a monster mustache"

    mario["hasPart"] = {"Head": {"face": ["mustache", "mustache", "mustache"], "others": "ears"},
                        "arms": ["left arm", "right arm"],
                        "clothes": 0,
                        "best fiend": green_mario,
                        "friends": [blue_mario, green_mario]}

    mario["hasVersion"] = [1968, blue_mario, green_mario, "some strange version with a flying pumpkin"]

    return mario


def create_alternative_term_dict() -> dict:

    return {"Dict": {
        "Name": "identifier",
        "Short description": "description",
        "Publisher": "publisher"
        },
        "List": ["identifier", "description", "publisher"],
        "Tuple": ("identifier", "description", "publisher")
    }


class TestMetaData2Markdown(unittest.TestCase):

    def out_dir(self) -> str:
        return os.path.join(os.getcwd(), "tests", "outputfiles")

    def ensure_right_directory(self):
        err_msg = "Testing is not executed in the root directory of dublincore"
        self.assertTrue(os.path.isdir(os.path.join(os.getcwd(), "tests")), err_msg)
        self.assertTrue(os.path.isdir(os.path.join(os.getcwd(), "dublincore")), err_msg)

        path = os.path.join(os.getcwd(), "tests", "outputfiles")
        if not os.path.exists(path):
            os.mkdir(path)

    def ensure_output_directory_empty(self):
        self.ensure_right_directory()
        if not os.path.exists(self.out_dir()):
            os.mkdir(self.out_dir())
        self.assertEqual(len(os.listdir(self.out_dir())), 0, "The output directory is not empty")

    def clean_output_files(self):
        self.ensure_right_directory()
        for file in os.listdir(self.out_dir()):
            file_path = os.path.join(self.out_dir(), file)
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.remove(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        self.ensure_output_directory_empty()

    def clean_wrong_file(self, filename: str):
        path = os.path.join(os.getcwd(), filename)
        if os.path.isfile(path):
            os.remove(path)
        self.assertFalse(os.path.isfile(path))

    def test_call(self):
        meta_data = MetaData()
        self.assertIsNotNone(metadata2markdown(meta_data),
                             "Calling metadata2markdown with empty MetaData returned nothing")
        self.assertIsNotNone(metadata2markdown(meta_data, None),
                             "Calling metadata2markdown with empty MetaData returned nothing")

    def test_call_with_data(self):
        meta_data = create_meta_data()
        self.assertIsNotNone(metadata2markdown(meta_data),
                             "Calling metadata2markdown example MetaData returned nothing")
        self.assertIsNotNone(metadata2markdown(meta_data, None),
                             "Calling metadata2markdown example MetaData returned nothing")

    def test_argument_unchanged(self):
        meta_data = create_meta_data()
        meta_data_backup = meta_data.copy()
        self.assertEqual(meta_data, meta_data_backup)
        self.assertIsNot(meta_data, meta_data_backup)
        self.assertIsNotNone(metadata2markdown(meta_data),
                             "Calling metadata2markdown example MetaData returned nothing")
        self.assertEqual(meta_data, meta_data_backup)

    def test_structure_no_write(self):
        meta_data = MetaData()

        self.clean_wrong_file(str(meta_data))

        reference_path = os.path.join("tests", "inputfiles", "reference_write_markdown.md")
        expected_lines = read_file(reference_path)

        output_lines = metadata2markdown(meta_data, None, None, "--highlight-missing")

        self.assertEqual(expected_lines, output_lines,
                         "Structure of Markdown File not returned correctly by metadata2markdown.\n" +
                         " ==== RETURNED STRUCTURE: ====\n" +
                         "\n".join(output_lines) +
                         " ====    EXPECTED:        ====\n" +
                         "\n".join(expected_lines) + "\n")

        self.assertFalse(os.path.isfile(os.path.join(os.getcwd(), str(meta_data))),
                         "File has been written out, but shouldn't")
        self.clean_wrong_file(str(meta_data))

    def test_write_md(self):
        self.clean_output_files()

        meta_data = MetaData()

        path = os.path.join("tests", "outputfiles", "test_write_markdown.md")
        reference_path = os.path.join("tests", "inputfiles", "reference_write_markdown.md")

        self.assertNotEqual(path, reference_path)
        if os.path.isfile(path):
            os.remove(path)
        self.assertFalse(os.path.isfile(path), "Test failed because clearing the output file did not work")
        output_lines = metadata2markdown(meta_data, path, None, "--highlight-missing")
        self.assertTrue(os.path.isfile(path), "Markdown file was not written")

        written_lines = read_file(path)
        expected_lines = read_file(reference_path)

        self.assertEqual(written_lines, expected_lines)
        self.assertEqual(output_lines, expected_lines)
        self.clean_output_files()

    def test_no_write_with_other_argument(self):

        self.clean_output_files()

        meta_data = MetaData("Example_metadata")

        path_print = os.path.join("tests", "outputfiles", "--print")
        path_example = os.path.join("tests", "outputfiles", "example1.md")

        self.assertFalse(os.path.isfile(path_print))
        self.assertFalse(os.path.isfile(path_example))

        metadata2markdown(meta_data, "--print")

        self.assertFalse(os.path.isfile(path_print))
        self.assertFalse(os.path.isfile(path_example))

        metadata2markdown(meta_data, path_example, "--print")

        self.assertFalse(os.path.isfile(path_print))
        self.assertTrue(os.path.isfile(path_example))

        metadata2markdown(meta_data, None, {"Chapter1": ["abstract", "description"]}, "--print")

        self.assertFalse(os.path.isfile(path_print))
        self.assertTrue(os.path.isfile(path_example))

        self.clean_output_files()

    def test_write_md_with_data(self):
        self.clean_output_files()

        meta_data = create_meta_data()
        path = os.path.join("tests", "outputfiles", "test_write_markdown_data.md")
        reference_path = os.path.join("tests", "inputfiles", "reference_write_markdown_data.md")

        self.assertNotEqual(path, reference_path)
        if os.path.isfile(path):
            os.remove(path)
        self.assertFalse(os.path.isfile(path), "Test failed because clearing the output file did not work")
        output_lines = metadata2markdown(meta_data, path, None, "--highlight-missing")
        self.assertTrue(os.path.isfile(path), "Markdown file was not written")

        written_lines = read_file(path)
        expected_lines = read_file(reference_path)

        self.assertEqual(written_lines, expected_lines)
        self.assertEqual(output_lines, expected_lines)
        self.clean_output_files()

    def test_write_md_data_alternative_term_dict(self):
        self.clean_output_files()

        meta_data = create_meta_data()
        path = os.path.join("tests", "outputfiles", "test_write_markdown_termdict.md")
        reference_path = os.path.join("tests", "inputfiles", "reference_write_markdown_termdict.md")

        self.assertNotEqual(path, reference_path)
        if os.path.isfile(path):
            os.remove(path)
        self.assertFalse(os.path.isfile(path), "Test failed because clearing the output file did not work")
        output_lines = metadata2markdown(meta_data, path, create_alternative_term_dict(), "--highlight-missing")
        self.assertTrue(os.path.isfile(path), "Markdown file was not written")

        written_lines = read_file(path)
        expected_lines = read_file(reference_path)

        self.assertEqual(written_lines, expected_lines)
        self.assertEqual(output_lines, expected_lines)
        self.clean_output_files()

    def test_write_md_with_yaml(self):
        self.clean_output_files()

        meta_data = create_meta_data()
        path = os.path.join("tests", "outputfiles", "test_write_markdown_yaml.md")
        reference_path = os.path.join("tests", "inputfiles", "reference_write_markdown_yaml.md")

        self.assertNotEqual(path, reference_path)
        if os.path.isfile(path):
            os.remove(path)
        self.assertFalse(os.path.isfile(path), "Test failed because clearing the output file did not work")
        output_lines = metadata2markdown(meta_data, path, None, "--include-yaml")
        self.assertTrue(os.path.isfile(path), "Markdown file was not written")

        written_lines = read_file(path)
        expected_lines = read_file(reference_path)

        self.assertEqual(written_lines, expected_lines)
        self.assertEqual(output_lines, expected_lines)
        self.clean_output_files()


if __name__ == '__main__':
    unittest.main()
