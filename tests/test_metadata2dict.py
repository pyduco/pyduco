
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


from dublincore.metadata2dict import *
import unittest


def create_mapped_meta_data() -> MetaData:
    meta_data = MetaData()
    meta_data["mappedKeyOne"] = None
    meta_data["mappedKey2"] = ["huhu"]
    meta_data["adjustedAdditionalKey"] = 56.123
    meta_data["transferredListDictKey"] = [{"name": "inner_name"}, 456]
    return meta_data


def create_deeply_non_mapped_meta_data() -> MetaData:
    meta_data = MetaData()
    meta_data["some_key"] = 456
    meta_data["unknown_key"] = None

    list_key_meta_data = MetaData()
    list_key_meta_data["identifier"] = "list_key_0"
    list_key_meta_data["Hallo2"] = "Hi!2"
    meta_data["list_key"] = [list_key_meta_data]

    dict_key_meta_data = MetaData()
    dict_key_meta_data["Hallo"] = "Hi!"
    dict_key_meta_data["identifier"] = "level2"
    meta_data["dict_key"] = dict_key_meta_data
    return meta_data


def create_deeply_mapped_meta_data() -> MetaData:
    meta_data = MetaData()
    meta_data["mappedKeyOne"] = None
    meta_data["mappedKey2"] = ["huhu"]
    meta_data["adjustedAdditionalKey"] = 56.123

    inner_meta_data = MetaData()
    inner_meta_data["identifier"] = "inner_name"
    meta_data["transferredListDictKey"] = [inner_meta_data, 456]
    return meta_data


def create_dict_for_mapping() -> dict:
    dictionary = dict()
    dictionary["__key-1__"] = None
    dictionary["key--two   "] = ["huhu"]
    dictionary["addiTIOnal_key"] = 56.123
    dictionary["\tlist_dict_key"] = [{"name": "inner_name"}, 456]
    return dictionary


def create_dict_no_mapping() -> dict:
    dictionary = dict()
    dictionary["some_key"] = 456
    dictionary["dict_key"] = {"Hallo": "Hi!", "identifier": "level2"}
    dictionary["list_key"] = [{"Hallo2": "Hi!2"}]
    dictionary["unknown_key"] = None
    return dictionary


def create_term_map() -> dict:
    return {
        "key1": "mappedKeyOne",
        "keytwo": "mappedKey2",
        "additionalkey": "adjustedAdditionalKey",
        "listdictkey": "transferredListDictKey",
        "name": "identifier"
    }


class TestMetaData2Dict(unittest.TestCase):

    def test_call(self):
        self.assertEqual(dict2metadata(dict()), MetaData(),
                         "Call dict2metadata without map argument failed")
        self.assertEqual(dict2metadata(dict(), None), MetaData(),
                         "Call dict2metadata without None as map argument failed")
        self.assertEqual(dict2metadata(dict(), dict()), MetaData(),
                         "Call dict2metadata with empty map argument failed")
        self.assertEqual(dict2metadata(dict(), create_term_map()), MetaData(),
                         "Call dict2metadata with map argument failed")

    def test_no_map(self):
        dictionary = create_dict_no_mapping()
        term_map = create_term_map()
        meta_data = MetaData().update(dictionary)

        meta_data_from_dict = dict2metadata(dictionary, term_map)

        self.assertEqual(meta_data_from_dict, meta_data)
        # test that dictionary has not been changed
        self.assertEqual(dictionary, create_dict_no_mapping())
        self.assertEqual(term_map, create_term_map())

    def test_map(self):
        dictionary = create_dict_for_mapping()
        term_map = create_term_map()
        meta_data = create_mapped_meta_data()

        self.assertEqual(dict2metadata(dictionary, term_map), meta_data)
        # test that dictionary has not been changed
        self.assertEqual(dictionary, create_dict_for_mapping())
        self.assertEqual(term_map, create_term_map())

    def test_map_and_nomap(self):
        dict_for_mapping = create_dict_for_mapping()
        dict_no_mapping = create_dict_no_mapping()
        dictionary = {**dict_for_mapping, **dict_no_mapping}
        term_map = create_term_map()
        meta_data = create_mapped_meta_data().update(dict_no_mapping)

        tmp = dict2metadata(dictionary, term_map)
        self.assertEqual(tmp, meta_data)
        # test that dictionary has not been changed
        self.assertEqual(dictionary, {**dict_for_mapping, **dict_no_mapping})
        self.assertEqual(term_map, create_term_map())

    def test_deep_call(self):
        self.assertEqual(deep_dict2metadata(dict()), MetaData(),
                         "Call deep_dict2metadata without map argument failed")
        self.assertEqual(deep_dict2metadata(dict(), None), MetaData(),
                         "Call deep_dict2metadata without None as map argument failed")
        self.assertEqual(deep_dict2metadata(dict(), dict()), MetaData(),
                         "Call deep_dict2metadata with empty map argument failed")
        self.assertEqual(deep_dict2metadata(dict(), create_term_map()), MetaData(),
                         "Call deep_dict2metadata with map argument failed")

    def test_deep_dict(self):
        dictionary = create_dict_no_mapping()
        term_map = create_term_map()
        meta_data = create_deeply_non_mapped_meta_data()
        meta_data_converted = deep_dict2metadata(dictionary, term_map)

        self.assertEqual(meta_data_converted, meta_data,
                         "\n==== CONVERTED ====\n" +
                         str(meta_data_converted.terms).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(meta_data.terms).replace(",", "\n"))
        # test that dictionary has not been changed
        self.assertEqual(dictionary, create_dict_no_mapping(),
                         "\n==== DICTIONARY ====\n" +
                         str(dictionary).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(create_dict_for_mapping()).replace(",", "\n"))
        self.assertEqual(term_map, create_term_map())

    def test_deep_dict_map(self):
        dictionary = create_dict_for_mapping()
        term_map = create_term_map()
        meta_data = create_deeply_mapped_meta_data()
        meta_data_converted = deep_dict2metadata(dictionary, term_map)

        self.assertEqual(meta_data_converted, meta_data,
                         "\n==== CONVERTED ====\n" +
                         str(meta_data_converted.terms).replace(",", "\n") +
                         "\n ----  inner  ----\n" +
                         str(meta_data_converted["transferredListDictKey"][0].terms).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(meta_data.terms).replace(",", "\n") +
                         "\n ----  inner  ----\n" +
                         str(meta_data["transferredListDictKey"][0].terms).replace(",", "\n"))
        # test that dictionary has not been changed
        self.assertEqual(dictionary, create_dict_for_mapping(),
                         "\n==== DICTIONARY ====\n" +
                         str(dictionary).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(create_dict_for_mapping()).replace(",", "\n"))
        self.assertEqual(term_map, create_term_map())

    def test_deep_deep(self):
        dictionary = {"level1_list": [{}, {}], "level1_dict": {"level2_dict": {"level3_dict": {}}}}
        dictionary_backup = deepcopy(dictionary)
        # double check
        self.assertEqual(dictionary, dictionary_backup)
        self.assertIsNot(dictionary, dictionary_backup)
        meta_data = MetaData()

        meta_data["level1_list"] = [MetaData(), MetaData()]
        meta_data["level1_list"][0]["identifier"] = "level1_list_0"
        meta_data["level1_list"][1]["identifier"] = "level1_list_1"

        meta_data["level1_dict"] = MetaData()
        meta_data["level1_dict"]["identifier"] = "level1_dict"
        meta_data["level1_dict"]["level2_dict"] = MetaData()
        meta_data["level1_dict"]["level2_dict"]["identifier"] = "level2_dict"
        meta_data["level1_dict"]["level2_dict"]["level3_dict"] = MetaData()
        meta_data["level1_dict"]["level2_dict"]["level3_dict"]["identifier"] = "level3_dict"

        term_map = create_term_map()
        meta_data_converted = deep_dict2metadata(dictionary, term_map)

        self.assertEqual(meta_data_converted, meta_data,
                         "\n==== CONVERTED ====\n" +
                         str(meta_data_converted.terms).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(meta_data.terms).replace(",", "\n"))
        # test that dictionary has not been changed
        self.assertEqual(dictionary, dictionary_backup,
                         "\n==== DICTIONARY ====\n" +
                         str(dictionary).replace(",", "\n") +
                         "\n==== ORIGINAL ====\n" +
                         str(create_dict_for_mapping()).replace(",", "\n"))
        self.assertEqual(term_map, create_term_map())


if __name__ == '__main__':
    unittest.main()
