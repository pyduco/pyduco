
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import datetime
import os
import unittest
from dublincore.metadata import MetaData
from dublincore.metadata2yaml import *


def create_study():
    my_meta = MetaData()
    my_meta.date = "31.12.1996"
    my_meta.creator = "Me"
    my_meta.dateCreated = "today"
    my_meta.addRelationIsPartOf("Top-level study")
    my_meta.addRelationIsPartOf("Topper-level study")
    my_meta.addRelationIsPartOf("Topmost-level study")
    my_meta.addRelationReferences("Awesome paper from literature. DOI: ...")
    my_meta.identifier = "epic Study"
    return my_meta


class TestMetaData2Yaml(unittest.TestCase):

    def ensure_right_directory(self):
        """
        Check if we are in the root directory of pyDuCo before we start writing stuff onto our precious storage disk
        """
        self.assertTrue(os.path.isdir(os.path.join(os.getcwd(), "dublincore")), "Directory dublincore not found. Are "
                                                                                "you sure that the test is executed in "
                                                                                "the root directory of pyDuCo?")
        self.assertTrue(os.path.isdir(os.path.join(os.getcwd(), "tests")), "Directory dublincore not found. Are "
                                                                           "you sure that the test is executed in "
                                                                           "the root directory of pyDuCo?")

        path = os.path.join(os.getcwd(), "tests", "outputfiles")
        if not os.path.exists(path):
            os.mkdir(path)

    def test_raises_file_not_found(self):
        self.assertRaises(FileNotFoundError, yaml2metadata, "Not_existing_file.file")

    def test_raises_directory_not_found(self):
        self.assertRaises(FileNotFoundError, metadata2yaml, MetaData(), "Not_existing_directory/file")

    def test_raises_faulty_yaml(self):
        self.assertRaises(BaseException, yaml2metadata, os.path.join("tests", "inputfiles", "test_faulty_yaml.yaml"))

    def test_read_yaml(self):
        self.ensure_right_directory()

        my_meta = create_study()
        my_meta_read_from_file = yaml2metadata(os.path.join("tests", "inputfiles", "test_read_yaml.yaml"))

        self.assertEqual(my_meta, my_meta_read_from_file)

    def test_write_yaml(self):
        self.ensure_right_directory()
        my_meta = create_study()
        my_meta.dateModified = str(datetime.datetime.now())
        my_meta_backup = my_meta.copy()   # copy saves state
        path = os.path.join("tests", "outputfiles", "test_write_yaml.yaml")
        if os.path.isfile(path):
            os.remove(path)

        # Write to file
        self.assertFalse(os.path.isfile(path), "Test failed because clearing the output file did not work")
        metadata2yaml(my_meta, path)
        self.assertTrue(os.path.isfile(path), "Yaml file was not written")
        # assure that my_meta has not been changed by the procedure
        self.assertEqual(my_meta, my_meta_backup)

        my_meta_read_from_written = yaml2metadata(path)

        self.assertEqual(my_meta, my_meta_read_from_written)

        if os.path.isfile(path):
            os.remove(path)

    def test_write_only_filename(self):
        self.ensure_right_directory()

        filename = "test_write_yaml.yaml"

        if os.path.isfile(filename):
            os.remove(filename)

        # Write to file
        self.assertFalse(os.path.isfile(filename), "Test failed because clearing the output file did not work")
        metadata2yaml(MetaData(), filename)
        self.assertTrue(os.path.isfile(filename), "Yaml file was not written")

        if os.path.isfile(filename):
            os.remove(filename)


if __name__ == '__main__':
    unittest.main()
