
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import unittest
import os.path

import dublincore


class TestCrawlEnvironment(unittest.TestCase):

    FILE_NAME = "environment.md"

    def working_dir(self) -> str:
        return os.path.join(os.getcwd(), "tests", "crawl_environment")

    def file_exists(self) -> bool:
        return os.path.isfile(os.path.join(self.working_dir(), self.FILE_NAME))

    def clean_up(self):
        if self.file_exists():
            os.remove(os.path.join(self.working_dir(), self.FILE_NAME))

    def ensure_right_directory(self):
        self.assertTrue(os.path.isdir(self.working_dir()), "Test directory 'crawl_environment' not found. Make sure that"
                                                           " the tests are executed in the pyDuCo root directory")

    def test_crawl_environment(self):
        self.ensure_right_directory()
        self.clean_up()

        self.assertFalse(self.file_exists())
        meta = dublincore.environ2metadata()
        self.assertIsNotNone(meta)
        dublincore.metadata2markdown(meta, os.path.join(self.working_dir(), self.FILE_NAME), None, "--include-yaml")
        self.assertTrue(self.file_exists())

        self.clean_up()


if __name__ == '__main__':
    unittest.main()
