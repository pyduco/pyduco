# It's-a me, Mario!

The coolest Italian guy with the monster mustache

## Dict

|                   |                                                   |
|-------------------|---------------------------------------------------|
| Name              | It's-a me, Mario!                                 |
| Short description | The coolest Italian guy with the monster mustache |
| Publisher         | Anonymous Japanese company                        |

## List

It's-a me, Mario!

The coolest Italian guy with the monster mustache

Anonymous Japanese company

## Tuple

|             |                                                   |
|-------------|---------------------------------------------------|
| identifier  | It's-a me, Mario!                                 |
| description | The coolest Italian guy with the monster mustache |
| publisher   | Anonymous Japanese company                        |
