# It's-a me, Mario!

The coolest Italian guy with the monster mustache

## Date

|       |         |
|-------|---------|
| date  | MISSING |
| valid | MISSING |

## Overview

|            |                   |
|------------|-------------------|
| identifier | It's-a me, Mario! |
| title      | MISSING           |
| subject    | MISSING           |
| format     | MISSING           |
| language   | MISSING           |
| type       | MISSING           |

## Validity

|          |         |
|----------|---------|
| coverage | MISSING |

## Relations

|        |         |
|--------|---------|
| Source | MISSING |

## Parts

|            |                                              |                                             |
|------------|----------------------------------------------|---------------------------------------------|
| Head       | face: mustache and 2 more<br>others: ears    |                                             |
| arms       | left arm<br>right arm                        |                                             |
| clothes    | 0                                            |                                             |
| best fiend | [Luigi](Luigi)                               | A green Italian guy with a monster mustache |
| friends    | [Blue Mario](Blue%20Mario)<br>[Luigi](Luigi) |                                             |

## Versions

|                                            |                                             |
|--------------------------------------------|---------------------------------------------|
| 1968                                       |                                             |
| [Blue Mario](Blue%20Mario)                 | A blue Italian guy with a monster mustache  |
| [Luigi](Luigi)                             | A green Italian guy with a monster mustache |
| some strange version with a flying pumpkin |                                             |

## Other formats

- Nintendo
- PC
- Wi

## Referenced by

123456789

## Requires

|            |              |
|------------|--------------|
| identifier | Mario's Cart |
| color      | red          |
| speed      | 99999        |

## Parties

|             |                            |
|-------------|----------------------------|
| creator     | MISSING                    |
| contributor | MISSING                    |
| publisher   | Anonymous Japanese company |

## Rights

|        |         |
|--------|---------|
| rights | MISSING |
