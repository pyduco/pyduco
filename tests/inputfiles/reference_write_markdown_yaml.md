---
description: The coolest Italian guy with the monster mustache
hasFormat:
- Nintendo
- PC
- Wi
hasPart:
  Head:
    face:
    - mustache
    - mustache
    - mustache
    others: ears
  arms:
  - left arm
  - right arm
  best fiend: &id001 !!python/object:dublincore.metadata.MetaData
    obligatory_terms:
    - contributor
    - coverage
    - creator
    - date
    - description
    - format
    - identifier
    - language
    - publisher
    - rights
    - source
    - subject
    - title
    - type
    - valid
    terms:
      description: A green Italian guy with a monster mustache
      hasFormat:
      - Nintendo
      - PC
      - Wi
      identifier: Luigi
      isReferencedBy: 123456789
      publisher: Anonymous Japanese company
      requires:
        color: red
        identifier: Mario's Cart
        speed: 99999
  clothes: 0
  friends:
  - &id002 !!python/object:dublincore.metadata.MetaData
    obligatory_terms:
    - contributor
    - coverage
    - creator
    - date
    - description
    - format
    - identifier
    - language
    - publisher
    - rights
    - source
    - subject
    - title
    - type
    - valid
    terms:
      description: A blue Italian guy with a monster mustache
      hasFormat:
      - Nintendo
      - PC
      - Wi
      identifier: Blue Mario
      isReferencedBy: 123456789
      publisher: Anonymous Japanese company
      requires:
        color: red
        identifier: Mario's Cart
        speed: 99999
  - *id001
hasVersion:
- 1968
- *id002
- *id001
- some strange version with a flying pumpkin
identifier: It's-a me, Mario!
isReferencedBy: 123456789
publisher: Anonymous Japanese company
requires:
  color: red
  identifier: Mario's Cart
  speed: 99999
---
# It's-a me, Mario!

The coolest Italian guy with the monster mustache

## Overview

|            |                   |
|------------|-------------------|
| identifier | It's-a me, Mario! |

## Parts

|            |                                              |                                             |
|------------|----------------------------------------------|---------------------------------------------|
| Head       | face: mustache and 2 more<br>others: ears    |                                             |
| arms       | left arm<br>right arm                        |                                             |
| clothes    | 0                                            |                                             |
| best fiend | [Luigi](Luigi)                               | A green Italian guy with a monster mustache |
| friends    | [Blue Mario](Blue%20Mario)<br>[Luigi](Luigi) |                                             |

## Versions

|                                            |                                             |
|--------------------------------------------|---------------------------------------------|
| 1968                                       |                                             |
| [Blue Mario](Blue%20Mario)                 | A blue Italian guy with a monster mustache  |
| [Luigi](Luigi)                             | A green Italian guy with a monster mustache |
| some strange version with a flying pumpkin |                                             |

## Other formats

- Nintendo
- PC
- Wi

## Referenced by

123456789

## Requires

|            |              |
|------------|--------------|
| identifier | Mario's Cart |
| color      | red          |
| speed      | 99999        |

## Parties

|           |                            |
|-----------|----------------------------|
| publisher | Anonymous Japanese company |
