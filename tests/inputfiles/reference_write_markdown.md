# MISSING

MISSING

## Date

|       |         |
|-------|---------|
| date  | MISSING |
| valid | MISSING |

## Overview

|            |         |
|------------|---------|
| identifier | MISSING |
| title      | MISSING |
| subject    | MISSING |
| format     | MISSING |
| language   | MISSING |
| type       | MISSING |

## Validity

|          |         |
|----------|---------|
| coverage | MISSING |

## Relations

|        |         |
|--------|---------|
| Source | MISSING |

## Parties

|             |         |
|-------------|---------|
| creator     | MISSING |
| contributor | MISSING |
| publisher   | MISSING |

## Rights

|        |         |
|--------|---------|
| rights | MISSING |
