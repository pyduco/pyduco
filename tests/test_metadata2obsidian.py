
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import os.path
import shutil
import unittest
from dublincore.metadata2obsidian import *


def create_metadata_tree() -> MetaData:
    meta_data = MetaData()
    meta_data.identifier = "ParentMetaData"

    part1 = MetaData()
    part2 = MetaData()
    part1.identifier = "Part1"
    part2.identifier = "Part2"

    creator = MetaData()
    creator.identifier = "Me"
    creator.dateCreated = "my Birthday"

    meta_data.addRelationHasPart(part1)
    meta_data.addRelationHasPart(part2)

    meta_data.creator = creator

    return meta_data


class MyTestCase(unittest.TestCase):

    def clear_output_vault(self):

        self.ensure_right_directory()
        path = os.path.join(os.getcwd(), "tests", "outputfiles", "outputvault")

        if os.path.isdir(path):
            shutil.rmtree(path)

    def ensure_right_directory(self):
        self.assertTrue(os.path.exists(os.path.join(os.getcwd(), "tests")),
                        "test is not executed in the pyDuCo root directory")
        self.assertTrue(os.path.exists(os.path.join(os.getcwd(), "dublincore")),
                        "test is not executed in the pyDuCo root directory")

        path = os.path.join(os.getcwd(), "tests", "outputfiles")
        if not os.path.exists(path):
            os.mkdir(path)

    def ensure_directory_exists(self, path):
        if not os.path.exists(path):
            os.mkdir(path)
        self.assertTrue(os.path.exists(path), "Failed to create directory for testing: {0}".format(path))

    def test_standard_settings(self):
        self.ensure_right_directory()
        self.clear_output_vault()

        path = os.path.join("tests", "outputfiles", "outputvault")
        os.mkdir(path)
        self.assertTrue(os.path.isdir(path))

        meta_data = create_metadata_tree()
        metadata2obsidian(meta_data, path)

        self.assertTrue(os.path.isfile(os.path.join(path, "Me.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "Part1.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "Part2.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "ParentMetaData.md")))

        self.clear_output_vault()

    def test_create_subdirs(self):
        self.ensure_right_directory()
        self.clear_output_vault()

        path = os.path.join("tests", "outputfiles", "outputvault")
        os.mkdir(path)
        self.assertTrue(os.path.isdir(path))

        meta_data = create_metadata_tree()
        metadata2obsidian(meta_data, path, None, "--create-subdirectories")

        self.assertTrue(os.path.isdir(os.path.join(path, "hasPart")))
        self.assertTrue(os.path.isdir(os.path.join(path, "Me")))
        self.assertTrue(os.path.isfile(os.path.join(path, "hasPart", "Part1.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "hasPart", "Part2.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "Me", "Me.md")))

        self.clear_output_vault()

    def test_recursion(self):
        self.ensure_right_directory()
        self.clear_output_vault()
        path = os.path.join("tests", "outputfiles", "outputvault")
        os.mkdir(path)
        self.assertTrue(os.path.isdir(path))

        old_version = MetaData("v1.0")
        new_version = MetaData("v2.0")

        old_version.relationIsReplacedBy = new_version  # copied by reference
        new_version.relationReplaces = old_version

        metadata2obsidian(old_version, path)
        self.assertTrue(os.path.isfile(os.path.join(path, "v1.0.md")))
        self.assertTrue(os.path.isfile(os.path.join(path, "v2.0.md")))

        self.clear_output_vault()


if __name__ == '__main__':
    unittest.main()
