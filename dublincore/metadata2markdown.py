
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import os.path

import yaml
from dublincore.metadata import MetaData

# -1 -> deactivated
#  0 -> standard      [Display Text](URL)
#  1 -> obsidian      [[URL]]
MARKDOWN_LINK_STYLE = 0

# # ,,,,,,,,,,,,,,,,,,,,,,,,
# # |DEVELOPER INFORMATION:|
# # ''''''''''''''''''''''''
# #
# # This script converts entries of a MetaData / Dict into a semi-nice Markdown document.
# #
# # Which metadata terms are included into the Markdown document is defined by the "markdown_layout"-dict:
# #   - Each <key> in the markdown layout makes one chapter:
# #        <key> -> heading
# #        <val> -> aka. <terms> pointing to content, which is searched for in MetaData[term], aka. <data>
# #
# #   - Each chapter can contain the <data> from one or more <terms>
# #     The <data> is converted to Markdown
# #       - Single terms are provides as str, list[str] or tuple[str]
# #       - Multiple terms which are to be added sequentially in the chapter are provided as list[str]
# #       - Multiple terms which are to be added as one are provided as tuple[str]
# #
# #   - For each term, an appropriate Markdown representation is chosen based on the type of the metadata[term]
# #     If term is a tuple such that the terms are added as one, the metadata from the terms is connected to a dict
# #     first, which results a table: <term> <data>...
# #       - None, "", []            -> Skip or add as "MISSING", dependent on the flag --highlight-missing
# #       - str | numeric           -> add as simple text
# #       - MetaData                -> add as link
# #       - list[str | numeric]     -> add as bullet points
# #       - list[list]              -> add as bullet points, with list downgraded to str
# #       - list[dict]              -> add as bullet points, with dict downgraded to str
# #       - list[MetaData]          -> add as 2 column table: Identifier as Link, description
# #       - dict[str | numeric]     -> add as 2 column table: key, val
# #       - dict[list]              -> add as 2 column table: key, list downgraded to str
# #       - dict[dict]              -> add as 2 column table: key, dict downgraded to str
# #       - dict[MetaData]          -> add as 3 column table: Key, Identifier as Link, description
# #
# #   - If an iterable contains dicts or MetaData, these shall be written out in distinct files,
# #     which is why they are not handled here.
# #
# #   - Internally, objects are flagged as certain level:
# #       - Level 0: None, "", []                   -> may be raised to level 1 by --highlight-missing
# #       - Level 1: str | numeric
# #       - Level 2: list                           -> may be downgraded to level 1 if inside an iterable
# #       - Level 3: dict
# #       - Level 4: MetaData


# """Returns a dictionary, which assigns each dublin core term to a chapter of the markdown file."""
STANDARD_MARKDOWN_LAYOUT: dict = {
        "Abstract": "abstract",
        "Date": ("date", "valid", "available", "created", "submitted", "accepted", "issued", "copyrighted", "modified"),
        "Overview": ("identifier", "title", "alternative", "subject", "conformsTo", "extent", "format",
                     "instructionalMethod", "language", "type"),
        "Validity": ("coverage", "spatial", "temporal"),

        "Relations": {"Related to": "relation", "Source": "source", "Is part of": "isPartOf", "Is replaced by":
                      "isReplacedBy", "Is required by": "isRequiredBy", "Is a format of": "isFormatOf",
                      "Is a version of": "isVersionOf", "Replaces": "replaces", "References": "references"},
        "Parts": "hasPart",
        "Versions": "hasVersion",
        "Other formats": "hasFormat",
        "Referenced by": "isReferencedBy",
        "Requires": "requires",
        "Accretion": ("accrualMethod", "accrualPeriodicity", "accrualPolicy"),
        "Parties": ("creator", "contributor", "publisher", "audience", "mediator", "medium"),
        "Rights": ("license", "rights", "rightsHolder", "accessRights", "provenance",
                   "bibliographicCitation")
    }


def _convert_to_single_line_str(_obj: object) -> str:
    """Sub-function to get a naming string out of an object"""
    try:
        if isinstance(_obj, list):
            return _convert_to_single_line_str(_obj[0]) + " and " + str(len(_obj) - 1) + " more"
        elif isinstance(_obj, MetaData):
            return _create_link(str(_obj))
        elif isinstance(_obj, dict):
            if "name" in _obj:
                return _create_link(_obj["name"])
            elif "identifier" in _obj:
                return _create_link(_obj["identifier"])
            else:
                return "Dict with {0} entries".format(len(_obj))
        else:
            return str(_obj)
    except NotImplementedError:
        # This is returned when the str() function can not be used on the object.
        return "UNABLE TO PROCESS OBJECT"


def _get_description(_obj: object) -> str:
    """Sub-function to get a descriptive string out of an object"""
    if isinstance(_obj, MetaData):
        d = _obj.description
        return d if d is not None else ""
    else:
        return ""


def _get_metadata_by_term(_meta_data: MetaData, term: str | tuple[str] | dict):
    """
        Looks up the requested term in the metadata obj. and returns the data if found.
        If term is a set of terms, they are connected to a dict.
    """
    if type(term) == tuple:
        d = dict()
        for sub_term in term:
            val = _get_metadata_by_term(_meta_data, sub_term)
            if _complexity_level(val) > 0:
                d[sub_term] = val
        if len(d.keys()) > 0:
            return d
        else:
            return None

    elif type(term) == dict:
        d = dict()
        for sub_term in term.keys():
            val = _get_metadata_by_term(_meta_data, term[sub_term])
            if _complexity_level(val) > 0:
                d[sub_term] = val
        if len(d.keys()) > 0:
            return d
        else:
            return None

    elif term in _meta_data and _complexity_level(_meta_data[term]) > 0:
        return _meta_data[term]
    else:
        return None


def _create_chapter(_meta_data: MetaData, _terms: list[str] | str | tuple[str], heading: str) -> list[str] | None:

    # Create the heading and an empty line
    chapter: list[str] = ["## {0}".format(heading), ""]

    # ensure that we can iterate over _terms
    if type(_terms) != list:
        _terms = [_terms]
    # iterate over all the data and append the Markdown stuff to the chapter
    for term in _terms:
        data = _get_metadata_by_term(_meta_data, term)
        if data is not None:
            chapter.extend(_create_markdown_representation(data))
            chapter.append("")

    if len(chapter) > 2:
        return chapter
    else:
        return None


def _create_markdown_representation(data) -> list[str]:
    level = _complexity_level(data)
    if level == 1:
        return [_convert_to_single_line_str(data)]
    elif level == 2:
        # If there is MetaData in the list
        if _complexity_level_of_elements(data) > 3:
            d = dict()
            for element in data:
                d[_convert_to_single_line_str(element)] = _get_description(element)
            return _create_table(d)
        # Only other data formats in the list
        else:
            return _create_bullet_points(data)
    elif level == 3:
        return _create_table(data)


def _create_bullet_points(data: list) -> list[str]:
    return ["- {0}".format(_convert_to_single_line_str(element)) for element in data]


def _create_table(data: dict) -> list[str]:
    """
    Sub-function to create a chapter with a table.
    The table has 3 columns:

    | key1 | value1 | description of value1 |
    | key2 | value2 |                       | <-- no description found
    | key3 | value3 | description of value4 |
    """

    col1: list[str] = []
    col2: list[str] = []
    col3: list[str] = []

    for key in data:
        col1.append(key)
        col2.append(_create_table_lines(data[key]))
        col3.append(_get_description(data[key]))

    # Width of the columns
    widths: list[int] = [max(len(item) for item in col1),
                         max(len(item) for item in col2),
                         max(len(item) for item in col3)]

    if _complexity_level_of_elements(data) < 4:
        t = ["| {0} | {1} |".format(" " * widths[0], " " * widths[1]),
             "|-{0}-|-{1}-|".format("-" * widths[0], "-" * widths[1])]
        t.extend(["| {0} | {1} |".format(c1.ljust(widths[0]), c2.ljust(widths[1])) for (c1, c2) in zip(col1, col2)])
        return t
    else:
        t = ["| {0} | {1} | {2} |".format(" " * widths[0], " " * widths[1], " " * widths[2]),
             "|-{0}-|-{1}-|-{2}-|".format("-" * widths[0], "-" * widths[1], "-" * widths[2])]
        t.extend(["| {0} | {1} | {2} |".format(c1.ljust(widths[0]), c2.ljust(widths[1]), c3.ljust(widths[2]))
                  for (c1, c2, c3) in zip(col1, col2, col3)])
        return t


def _create_table_lines(_obj) -> str:
    if isinstance(_obj, list):
        return "<br>".join([_convert_to_single_line_str(element) for element in _obj])
    if isinstance(_obj, dict) and "name" not in _obj and "identifier" not in _obj:
        return "<br>".join(["{0}: {1}".format(key, _convert_to_single_line_str(_obj[key])) for key in _obj])
    else:
        return _convert_to_single_line_str(_obj)


def _create_link(text: str) -> str:
    """Creates a Markdown link (Markdown, Obsidian style or plain text) dependent on global setting"""
    global MARKDOWN_LINK_STYLE
    if MARKDOWN_LINK_STYLE == 0:
        return "[{0}]({1})".format(text, text.replace(" ", "%20"))
    elif MARKDOWN_LINK_STYLE == -1:
        return text
    elif MARKDOWN_LINK_STYLE == 1:
        return "[[{0}]]".format(text)
    else:
        raise ValueError("Why is the global variable MARKDOWN_LINK_STYLE not -1, 0 or 1?")


def _complexity_level(_obj) -> int:
    if _obj is None or \
            isinstance(_obj, (str, list, dict)) and not _obj:
        return 0

    if isinstance(_obj, (str, int, float)):
        return 1
    elif isinstance(_obj, list):
        if _complexity_level_of_elements(_obj) == 0:
            return 0  # empty list
        else:
            return 2
    elif isinstance(_obj, dict):
        if _complexity_level_of_elements(_obj) == 0:
            return 0  # empty dict
        else:
            return 3
    elif isinstance(_obj, MetaData):
        return 4

    return 5


def _complexity_level_of_elements(data: list | dict | MetaData) -> int:
    if type(data) == list:
        return max(_complexity_level(element) for element in data)
    else:
        return max(_complexity_level(data[element]) for element in data)


def metadata2markdown(meta_data: MetaData, path: str = None, markdown_layout: dict = None, *args) -> list[str]:
    """
    metadata2markdown(my_metadata, "my_markdown_file.md")
    my_list_of_str = metadata2markdown(my_metadata)

    Get Markdown representation from a MetaData object.

        argument1:              MetaData object
        argument2 (optional):   File path

        output argument:        List of string. Each item in the list represents one line of Markdown code.

    Possible arguments:

        --dublin-core-only      Include only metadata terms which are part of the dublin core specification.
        --highlight-missing     Include and highlight obligatory metadata terms, which are not set.
        --include-yaml          Include the yaml representation of the Markdown object in the preface of the Markdown.
        --links-deactivated     Use plain text instead of Markdown links
        --links-obsidian        Use [[url]] links instead of [alt-text](url) links.
        --obligatory-only       Include only metadata terms which are obligatory by the employed specification.
        --print                 Print the final Markdown document to the standard out stream
    """

    # often it happens that the positional arguments are forgotten. Handle this
    if isinstance(path, str) and path.startswith("--"):
        args += (path, )
        path = None

    if isinstance(markdown_layout, str) and markdown_layout.startswith("--"):
        args += (markdown_layout, )
        markdown_layout = None

    # CLEAR TERMS
    meta = meta_data.copy()
    if "--obligatory-only" in args:
        meta.clear_non_obligatory_terms()
    elif "--dublin-core-only" in args:
        meta.clear_non_dublin_core_terms()

    # HIGHLIGHT MISSING
    if "--highlight-missing" in args:
        for obl_key in meta.obligatory_terms:
            if obl_key not in meta or _complexity_level(meta[obl_key]) == 0:
                meta[obl_key] = "MISSING"

    # SET LINKS
    global MARKDOWN_LINK_STYLE
    if "--links-deactivated" in args:
        MARKDOWN_LINK_STYLE = -1
    elif "--links-obsidian" in args:
        MARKDOWN_LINK_STYLE = 1
    else:
        MARKDOWN_LINK_STYLE = 0

    document: list[str] = []

    # INCLUDE THE YAML PREFACE
    if "--include-yaml" in args:
        document.append("---")
        document.extend(yaml.dump(meta.terms.copy()).split("\n")[0:-1])
        document.append("---")

    # CREATE THE FIRST CHAPTER
    document.extend(["# {0}".format(str(meta)), ""])
    if "description" in meta:
        document.extend([meta.description, ""])

    # Iteratively create the chapters
    if markdown_layout is None:
        markdown_layout = STANDARD_MARKDOWN_LAYOUT
    for heading in markdown_layout:
        c = _create_chapter(meta, markdown_layout[heading], heading)
        if c is not None:
            document.extend(c)

    # CREATE THE CHAPTER 'ADDITIONAL INFORMATION'
    # document.chapters.append(_create_chapter(meta, meta.get_terms(), "Additional information"))

    if path is not None:
        if not os.path.isabs(path):
            path = os.path.join(os.getcwd(), path)

        with open(path, 'w') as file:
            file.writelines("\n".join(document))

    if "--print" in args:
        print("\n".join(document))

    return document
