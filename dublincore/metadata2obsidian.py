
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import os

from dublincore.metadata import MetaData
from dublincore.metadata2markdown import metadata2markdown


def _prepare_subdirectory(current_directory, sub_directory, create_if_not_existent: bool) -> str:
    """
    Checks the existence of a subdirectory in a directory and returns a path:
     - Not existent: Original directory
     - Existent:     Subdirectory
     - Not existent and create_if_not_existent flag is true: 
                     Creates the directory and returns its path
    """
    new_directory = os.path.join(current_directory, sub_directory)
    if os.path.isdir(new_directory):
        return new_directory
    elif create_if_not_existent:
        os.makedirs(new_directory)
        return new_directory
    else:
        return current_directory
    

def metadata2obsidian(meta_data: MetaData, directory: str = None, markdown_layout: dict = None, *args, **kwargs):
    """
    metadata2obsidian(my_metadata, "my_directory")

    Get Obsidian Vault representation from a MetaData object.

        argument1:              MetaData object
        argument2 (optional):   Vault path

    Crawls through the given MetaData object and all sub-MetaData objects and writes them out using metadata2markdown.
    The flag --obsidian-links is automatically used.

    Possible arguments:

        --use-subdirectories    If MetaData object "foo" has sub-MetaData objects "bar" and "baz", the markdown
                                files "bar.md" and "baz.md" are saved in the directories "bar" and "baz", if existent.
                                This is intended for usage with parameter studies in which each sub-object has its own
                                directory:
                                - dir main
                                  main.md
                                    - dir Setup1
                                      Setup1.md
                                      result_file.hdf
                                      input_file.yaml
                                    - dir Setup2
                                      Setup2.md
                                      ...
        --create-subdirectories Similar to --use-subdirectories, but directories are created if necessary
        --dublin-core-only      Include only metadata terms which are part of the dublin core specification.
        --include-yaml          Include the yaml representation of the Markdown object in the preface of the Markdown.
        --links-deactivated     Use plain text instead of Markdown links
        --obligatory-only       Include only metadata terms which are obligatory by the employed specification.
    """

    recursion_dict = kwargs["recursion_dict"] if "recursion_dict" in kwargs else []
    filename = str(meta_data)
    if filename in recursion_dict:
        return
    
    # Check the given directory
    current_dir = os.getcwd()
    if directory is not None:
        if os.path.isdir(directory):
            current_dir = directory
        else:
            raise NotADirectoryError("Path is given for Obsidian export but directory is not found: " + str(directory))

    # update args
    if "--links-obsidian" not in args:
        args += ("--links-obsidian", )

    # get some booleans
    create_subdirectories = "--create-subdirectories" in args
    use_subdirectories = "--use-subdirectories" in args or create_subdirectories
    
    file_path = os.path.join(current_dir, filename + ".md")
    recursion_dict.append(filename)
    metadata2markdown(meta_data, file_path, markdown_layout, *args)
    
    for term in meta_data:
        # Find the sub-MetaData
        if isinstance(meta_data[term], MetaData):
            
            # Go one directory level deeper, if necessary
            sub_dir = current_dir
            if use_subdirectories:
                sub_dir = _prepare_subdirectory(current_dir, str(meta_data[term]), create_subdirectories)

            # Recursively call this function
            metadata2obsidian(meta_data[term], sub_dir, markdown_layout, *args, recursion_dict=recursion_dict)

        # Other possibility: We find some list which contains sub-MetaData
        elif isinstance(meta_data[term], list):
            # Go one directory level deeper, if necessary
            sub_dir = current_dir
            if use_subdirectories:
                # This time, we use the term itself as subdirectory. e.g. "Setups"
                sub_dir = _prepare_subdirectory(current_dir,  term,  create_subdirectories)

            # Recursively call this function for all sub-MetaDatas.
            for item in meta_data[term]:
                if isinstance(item, MetaData):
                    metadata2obsidian(item, sub_dir, markdown_layout, *args, recursion_dict=recursion_dict)
