
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import datetime
import os

from dublincore.metadata import MetaData
import dublincore.metadata2dict as m2d


def obj2metadata(obj: object, term_map=None) -> MetaData:
    """
    my_meta_data = obj2metadata(any_object)

    This crawler extracts metadata from an arbitrary python object and stores it inside a MetaData object.
    If certain data is not found, the metadata entry will be left empty. This means that a MetaData object created by
    this crawler not necessarily filled.

        argument 1:             Python object
        argument 2 (optional):  term map

                                term map is a dict which gives the proper dublin core term for any key found in the
                                dictionary.It can also be loaded from a yaml file. If no argument is given, the standard
                                term map from standard_term_map.py is used, which considers common keys from python
                                classes, OS environment, and python global environment.

        output argument:       MetaData object.
    """
    if isinstance(obj, MetaData):
        return obj
    elif isinstance(obj, dict):
        return m2d.dict2metadata(obj)

    dict_tmp = dict()

    if hasattr(obj, '__len__'):
        dict_tmp["extend"] = len(obj)
    if hasattr(obj, '__name__'):
        dict_tmp["identifier"] = obj.__name__
    if hasattr(obj, '__class__'):
        dict_tmp["type"] = str(obj.__class__)
    if hasattr(obj, 'type'):
        dict_tmp["type"] = str(type(obj))
    if hasattr(obj, '__str__'):
        dict_tmp["description"] = obj.__str__()
    if hasattr(obj, 'super'):
        dict_tmp["isVersionOf"] = obj.super()

    dict_tmp["date"] = datetime.date.today()

    try:
        for key, val in obj:
            dict_tmp[key] = val
    except:
        try:
            for key in obj:
                dict_tmp[key] = obj[key]
        except:
            try:
                dict_tmp["hasPart"] = [item for item in obj]
            except:
                pass

    for term in dir(obj):
        try:
            if hasattr(obj, term) and not callable(getattr(obj, term, None)):
                dict_tmp[term] = getattr(obj, term, None)
        except:
            pass

    return m2d.dict2metadata(dict_tmp, term_map, "--only-mapped")


def environ2metadata() -> MetaData:
    """
    Crawls the current python os.environ for useful metadata. The found information is returned as a MetaData object.
    """
    return obj2metadata(os.environ)


