
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#

"""
This is the standard term_map for the conversion of dicts into the Dublin Core™ format. In times of duck programming and
everything-is-a-dict, the only thing we have to care about is: "Which python attribute contains useful information to
fill one of our beloved Dublin Core™ term with Meta Data?" Here, we collect all our ideas for later use in the crawlers.

usage:
OriginalPythonTerm -> Dublin Core compliant term

remember, that it is not necessary that the found attribute _exactly_ matches the OriginalPythonTerm in this list.
Therefore, there is no need to add len, __len__ and __len()__. These terms will be recognized as alike by the
term_variant_in_term_map() function in metadata2dict.py
"""

standard_term_map = {
    # some basic things
    "pwd": "spatial",
    "name": "identifier",
    "id": "identifier",
    "study": "identifier",
    "size": "extend",
    "length": "extend",
    "input": "source",
    "what": "description",
    "URL": "references",
    "super": "isVersionOf",
    "coauthor": "contributor",
    "model": "type",
    "year": "date",
    "brand": "creator",
    "author": "creator",
    "country": "spatial",
    "city": "spatial",
    "fileformat": "format",
    "norm": "conformsTo",
    "file": "source",
    "aka": "hasVersion",
    "alias": "hasVersion",
    "inner": "hasPart",
    "parent": "isVersionOf",
    "child": "hasVersion",
    "class": "type",
    "copyright": "rightsHolder",
    "credits": "contributor",
    "src": "source",
    "username": "creator",   # WINDOWS
    "user": "creator",       # LINUX
}
