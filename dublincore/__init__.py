
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


from dublincore.metadata import MetaData
from dublincore.metadata2dict import dict2metadata, deep_dict2metadata
from dublincore.metadata2yaml import yaml2metadata, metadata2yaml
from dublincore.metadata2markdown import metadata2markdown
from dublincore.metadata2obj import obj2metadata, environ2metadata
from dublincore.metadata2obsidian import metadata2obsidian
