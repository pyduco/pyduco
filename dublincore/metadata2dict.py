
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import os.path
from copy import deepcopy

from dublincore.metadata import MetaData
import yaml
from dublincore.standard_term_map import standard_term_map


def term_variant_in_term_map(term: str, term_map: dict) -> str | None:
    """
    This function takes a term as string and a term map as dict. It checks if the term or a alike term appears in the
    term map. If so, the first alike term from the term map is returned as string.
    If no alike term is found, None is returned

    A term in the term map is alike if it is compared to the original term:
        - identical
        - without leading and trailing chars:
            " ", "_", "-", "\t"
    """
    term_variant = term.strip(" \t")
    term_variant = term_variant.lower()
    term_variant = term_variant.replace("-", "")
    term_variant = term_variant.replace("_", "")
    if term_variant in term_map:
        return term_variant
    return None


def preprocess_term_map(term_map_in=None) -> dict:
    """
    This function preprocesses a term_map argument dependent on its type:
        None     -> Start with the standard term map
        dict     -> Start with an unchanged copy
        str/path -> Read dict from file

    In any case, the official dublin core terms are added to the term_map, such that e.g. any entry "date" during
    crawling a object for metadata will also appear in the MetaData entry "date". The final term_map is returned as a
    dict with the structure {str: str, str:str, ...}.
    """

    # Distinguish input types:
    # of type None
    if term_map_in is None:
        term_map_out = standard_term_map
    # of type dict
    elif isinstance(term_map_in, dict):
        term_map_out = term_map_in.copy()
    # of type string or path
    elif isinstance(term_map_in, str) or isinstance(term_map_in, os.PathLike):
        if os.path.isfile(term_map_in):
            with open(term_map_in, 'r') as file:
                text = file.readlines()
            term_map_out = yaml.safe_load("\n".join(text))
        else:
            raise FileNotFoundError("File " + str(term_map_in) + " not found")
    else:
        raise TypeError("Second argument term_map must either be a dictionary or a "
                        "path to a .yaml file containing a dictionary")

    # This includes all dublin core terms into the search.
    # So we will find already existing dublin core compliant metadata
    for term in MetaData.TERMS_DUBLIN_CORE_ALL:
        term_map_out[term] = term
    return term_map_out


def _deep_convert_single_object_if_convertible_to_dict(_obj: object, _term_map: dict = None) -> object:
    """
    Tries to interpret and convert a single object as/into a dict structure which can then be mapped into a MetaData.
    Returns the MetaData object. If no conversion is possible, the object is returned without modification
    """
    if isinstance(_obj, dict):
        return deep_dict2metadata(_obj, _term_map)
    elif isinstance(_obj, MetaData):
        return deep_dict2metadata(_obj.terms, _term_map)
    else:
        return _obj


def _add_missing_identifier_if_metadata(_meta: MetaData, _identifier: str):
    """
    This is a small utility function that takes an object and a string. Only if the object is of type metadata and the
    MetaData object does not have an identifier yet, the string is added to the MetaData as identifier.
    """
    if isinstance(_meta, MetaData) and "identifier" not in _meta:
        _meta.identifier = _identifier


def dict2metadata(dictionary: dict, term_map=None, *args) -> MetaData:
    """
    Transfer a dictionary into a MetaData object. The difference to MetaData.update(my_dict) is that a term map is used
    to bring the found data into the dublin core format.

    args:
        --only-mapped       All terms which are not identified as alike any Dublin Core Term, are omitted

    example:
    my_term_map = {"USERNAME": "creator", "__len__": "extent"}
    my_term_map_path = "term_map.yaml"
    my_metadata = dict2metadata(my_dict)
    my_metadata = dict2metadata(my_dict, my_term_map)
    my_metadata = dict2metadata(my_dict, my_term_map_path)

        argument 1:             dictionary
        argument 2 (optional):  term map

                                term map is a dict which gives the proper dublin core term for any key found in the
                                dictionary.It can also be loaded from a yaml file. If no argument is given, the standard
                                term map from standard_term_map.py is used, which considers common keys from python
                                classes, OS environment, and python global environment.

    If several data entries are found which would be mapped into the same dublin core term, the term first occurring
    in the term map is mapped into the dublin core terminology. The other data is included into the MetaData object
    without mapping, as long as the "--only-mapped" flag is not activated.

    For converting a Metadata object into a dict, use the builtin dict(my_metadata) function.
    """

    # Read the term map (from file or dict, whatever...) This is outsourced to this dedicated function
    term_map = preprocess_term_map(term_map)
    # Create an empty MetaData object
    meta_data = MetaData()

    include_all = "--only-mapped" not in args

    for term in dictionary:
        if not callable(dictionary[term]):
            # If an alike term is in the dublin core spec, we will find a term_variant str. If not, it is None
            term_variant = term_variant_in_term_map(term, term_map)
            if term_variant is not None:
                if term_map[term_variant] not in meta_data:
                    meta_data[term_map[term_variant]] = deepcopy(dictionary[term])
                else:
                    meta_data.add_to_term(term_map[term_variant], dictionary[term])
            # This one is a bit tricky, since it writes _every_ entry from the dict into the MetaData. That basically
            # means that the MetaData object does not only contain _meta_data, but also _data_.
            elif include_all and term not in meta_data:
                meta_data[term] = deepcopy(dictionary[term])
            elif include_all:
                meta_data.add_to_term(term, dictionary[term])

    return meta_data


def deep_dict2metadata(dictionary: dict, term_map=None, *args) -> MetaData:
    """
    Transfer a multi-level dictionary into an equivalent MetaData object. The algorithm recursively creeps through all
    sub-dictionaries and converts them into meta-data objects to bring the found data into the dublin core format.

    example:
    my_term_map = {"USERNAME": "creator", "__len__": "extent"}
    my_term_map_path = "term_map.yaml"
    my_metadata = dict2metadata(my_dict)
    my_metadata = dict2metadata(my_dict, my_term_map)
    my_metadata = dict2metadata(my_dict, my_term_map_path)

        argument 1:             dictionary
        argument 2 (optional):  term map

                                term map is a dict which gives the proper dublin core term for any key found in the
                                dictionary.It can also be loaded from a yaml file. If no argument is given, the standard
                                term map from standard_term_map.py is used, which considers common keys from python
                                classes, OS environment, and python global environment.

    If several data entries are found which would be mapped into the same dublin core term, the term first occurring
    in the term map is mapped into the dublin core terminology. The other data is included into the MetaData object
    without mapping.

    Inside one level of the deep dictionary, the conversion is executed using the dict2metadata function. This function
    is behaving correspondingly.
    """

    term_map = preprocess_term_map(term_map)

    # Map the current level onto a metadata object
    meta_data = dict2metadata(dictionary, term_map, *args)
    # Now go one level down and search for sub-dicts, -metadata and other convertible structures.
    # Recursively call this function to convert everything down to the deepest level.
    for term in meta_data:
        if isinstance(meta_data[term], list):
            # Case 1: If it is a relation object, with high probability we have a list of dicts or metadata.
            # Iterate through this
            my_list = meta_data[term]
            for i in range(0, len(my_list)):
                # List is mutable, so we can reference like that
                my_list[i] = _deep_convert_single_object_if_convertible_to_dict(my_list[i], term_map)
                _add_missing_identifier_if_metadata(my_list[i], term + "_" + str(i))
        else:
            # Case 2: Anything else
            meta_data[term] = _deep_convert_single_object_if_convertible_to_dict(meta_data[term], term_map)
            _add_missing_identifier_if_metadata(meta_data[term], term)

    return meta_data


