
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


from copy import deepcopy


class MetaData:

    OBLIGATORY_TERMS_DUBLIN_CORE = ["contributor", "coverage", "creator", "date", "description", "format", "identifier",
                                    "language", "publisher", "relation", "rights", "source", "subject", "title", "type",
                                    "valid"]

    OBLIGATORY_TERMS_STFS = ["contributor", "coverage", "creator", "date", "description", "format", "identifier",
                             "language", "publisher", "rights", "source", "subject", "title", "type",
                             "valid"]

    TERMS_DUBLIN_CORE_ALL = ["abstract", "accessRights", "accrualMethod", "accrualPeriodicity", "accrualPolicy",
                             "alternative", "audience", "available", "bibliographicCitation",  "conformsTo",
                             "contributor", "coverage", "created", "creator",  "date",
                             "accepted", "copyrighted", "submitted", "description",  "extent",
                             "format", "hasFormat", "hasPart", "hasVersion",  "identifier",
                             "instructionalMethod", "isFormatOf", "isPartOf", "isReferencedBy",  "isReplacedBy",
                             "isRequiredBy", "issued", "isVersionOf", "language",  "license",
                             "mediator", "medium", "modified", "provenance",  "publisher",
                             "references", "relation", "replaces", "requires",  "rights",
                             "rightsHolder", "source", "spatial", "subject",  "temporal",
                             "title", "type", "valid"]

    def add_term(self, term: str, value=None):
        """Add metadata by term and value. Existing data will be overwritten."""
        self[term] = value

    def is_adequately_filled(self) -> bool:
        """Return a boolean which indicates whether all required terms are filled to be compliant to the
        Dublin Core specification"""
        return all([term in self for term in self.obligatory_terms])

    def get_unfilled_terms(self) -> list[str]:
        """Return a list of string with the keywords of all unfilled terms which are required to be compliant to the
        Dublin Core specification"""
        return [term for term in self.iter_unfilled_terms()]

    def iter_unfilled_terms(self):
        """Iterate over all unfilled terms which are required to be compliant to the Dublin Core specification"""
        for term in self.obligatory_terms:
            if term not in self:
                yield term

    def get_terms(self) -> list[str]:
        """Return a list of str with the filled terms """
        return [str(k) for k in self.terms.keys()]

    def _set_term_ensure_list(self, term, value):
        """Sets a term and ensures that the applied value is a list.
        Use for terms which have multiple entries """
        if isinstance(value, list):
            self[term] = value
        else:
            self[term] = [value]

    def add_to_term(self, term, value):
        tmp = self[term] if term in self else []
        # ensure that we have a list here
        tmp = tmp if isinstance(tmp, list) else [tmp]

        if isinstance(value, list):
            tmp.extend(value)
        else:
            tmp.append(value)
        self[term] = tmp

    def __init__(self, identifier: str | None = None):
        self.terms: dict = dict()
        if identifier is not None:
            self.identifier = identifier
        self.obligatory_terms = self.OBLIGATORY_TERMS_STFS.copy()

    def __getitem__(self, item):
        return self.terms[item]

    @staticmethod
    def _deep_copy_if_not_meta_data(_obj: object) -> object:
        """
            Returns a deep copy of an object, if it is not of type MetaData
            If it _is_ of type MetaData, it returns a reference.
        """
        if isinstance(_obj, MetaData):
            return _obj
        elif isinstance(_obj, list):
            return [MetaData._deep_copy_if_not_meta_data(item) for item in _obj]
        elif isinstance(_obj, dict):
            return {key: MetaData._deep_copy_if_not_meta_data(_obj[key]) for key in _obj}
        else:
            return deepcopy(_obj)

    def __setitem__(self, term, value):
        self.terms[term] = MetaData._deep_copy_if_not_meta_data(value)

    def __str__(self):
        """
        Returns the identifier of the MetaData Object.
        """
        if "identifier" in self.terms.keys():
            return str(self.terms["identifier"])
        else:
            return "Unidentified metadata object"

    def __contains__(self, term):
        """
        Returns True if metadata is stored under the argument term. Else False.
        """
        return term in self.terms

    def __iter__(self):
        """iterate over all filled terms of the metadata. Returns a key-value pair as dict"""
        for term in self.terms:
            yield str(term)

    def __len__(self):
        return len(self.terms)

    def __copy__(self):
        other = MetaData()
        other.terms = self.terms.copy()
        other.obligatory_terms = self.obligatory_terms.copy()
        return other

    def __deepcopy__(self, memodict=None):
        memodict = memodict if memodict is not None else {}
        other = MetaData()
        other.terms = deepcopy(self.terms, memodict)
        other.obligatory_terms = self.obligatory_terms.copy()
        return other

    def __eq__(self, other):
        return isinstance(other, MetaData) and \
            self.terms == other.terms and \
            self.obligatory_terms == other.obligatory_terms

    def __ne__(self, other):
        return not self.__eq__(other)
    
    def __dict__(self):
        return {**self.terms, "obligatory_terms": self.obligatory_terms}

    def copy(self):
        """returns a deepcopy of the metadata object"""
        return self.__deepcopy__()

    def mini_copy(self):
        """returns a shrinked copy of the metadata object, which only keeps the identifier and the description.
        This can be used to enable linkage during write-out, avoiding reference loops or many copies of large
        MetaData"""
        out = MetaData()
        out.obligatory_terms = self.obligatory_terms.copy()
        out.identifier = self.identifier
        out.description = self.description
        out.obligatory_terms = []
        return out

    def update(self, other):
        """Integrate another key-value-pair object into the metadata object key by key.
        Existing data will be overwritten."""
        for key in other:
            self[key] = other[key]
        return self

    def stock_up(self, other):
        """Integrate another key-value-pair object into the metadata object key by key.
        Existing data will be kept."""
        for key in other:
            if key not in self:
                self[key] = other[key]
        return self

    def clear_non_obligatory_terms(self):
        """Removes all metadata from the MetaData object, which is not required by the applied specification.
        Returns the cleared MetaData object
        """
        for term in self.get_terms():
            if term not in self.obligatory_terms:
                self.terms.pop(term)
        return self

    def clear_non_dublin_core_terms(self):
        """Removes all metadata from the MetaData object, which is not in line with the dublin core terminology.
        Returns the cleared MetaData object
        """
        for term in self.get_terms():
            if term not in self.TERMS_DUBLIN_CORE_ALL:
                self.terms.pop(term)
        return self

    # ------------------------------------------------------------------------------------------------------------------
    #   PROPERTIES FOR ACCESS AND DOCUMENTATION OF TERMS
    # ------------------------------------------------------------------------------------------------------------------
    # make nomenclature available by properties. The properties simply access the corresponding terms in the dictionary
    # Intention
    #  - Programmers do not need to look up the naming convention but can easily use these properties
    #  - If the IDE supports auto-completion with function hints, the docstring will give programmers instructions which
    #    term to use to store their metadata
    #  - Add and clear relations without handling lists.
    #    (one relation can have multiple targets, which are stored as list in the dict)
    #  - In general, future changes in the naming convention will be possible without changes in the interface. However,
    #    the naming convention is intended to stay at DublinCore
    #  - terms beyond DublinCore can be added
    # ------------------------------------------------------------------------------------------------------------------
    # The docstrings in this part are the official definitions and comments from the Dublin Core™ term specifications
    # https://www.dublincore.org/specifications/dublin-core/dcmi-terms
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def abstract(self):
        """A summary of the resource."""
        return self.terms["abstract"] if "abstract" in self.terms else None

    @abstract.setter
    def abstract(self, value):
        self.terms["abstract"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def accessRights(self):
        """
        Information about who access the resource or an indication of its security status.
        ---
        Access Rights may include information regarding access or restrictions based on privacy,
        security, or other policies."""
        return self.terms["accessRights"] if "accessRights" in self.terms else None

    @accessRights.setter
    def accessRights(self, value):
        self.terms["accessRights"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def accrualMethod(self):
        """
        The method by which items are added to a collection.
        ---
        Recommended practice is to use a value from the Collection Description Accrual Method Vocabulary.
        https://dublincore.org/groups/collections/accrual-method/"""
        return self.terms["accrualMethod"] if "accrualMethod" in self.terms else None

    @accrualMethod.setter
    def accrualMethod(self, value):
        self.terms["accrualMethod"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def accrualPeriodicity(self):
        """
        The frequency with which items are added to a collection.
        ---
        Recommended practice is to use a value from the Collection Description Frequency Vocabulary.
        https://dublincore.org/groups/collections/frequency/"""
        return self.terms["accrualPeriodicity"] if "accrualPeriodicity" in self.terms else None

    @accrualPeriodicity.setter
    def accrualPeriodicity(self, value):
        self.terms["accrualPeriodicity"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def accrualPolicy(self):
        """
        The policy governing the addition of items to a collection.
        ---
        Recommended practice is to use a value from the Collection Description Accrual Policy Vocabulary.
        https://dublincore.org/groups/collections/accrual-policy/"""
        return self.terms["accrualPolicy"] if "accrualPolicy" in self.terms else None

    @accrualPolicy.setter
    def accrualPolicy(self, value):
        self.terms["accrualPolicy"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def alternative(self):
        """
        An alternative name for the resource.
        ---
        The distinction between titles and alternative titles is application-specific."""
        return self.terms["alternative"] if "alternative" in self.terms else None

    @alternative.setter
    def alternative(self, value):
        self.terms["alternative"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def audience(self):
        """
        A class of agents for whom the resource is intended or useful.
        ---
        Recommended practice is to use this property with non-literal values from a vocabulary of audience types."""
        return self.terms["audience"] if "audience" in self.terms else None

    @audience.setter
    def audience(self, value):
        self.terms["audience"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateAvailable(self):
        """
        Date that the resource became or will become available.
        ---
        Recommended practice is to describe the date, date/time,
        or period of time as recommended for the property Date."""
        return self.terms["available"] if "available" in self.terms else None

    @dateAvailable.setter
    def dateAvailable(self, value):
        self.terms["available"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def bibliographicCitation(self):
        """
        A bibliographic reference for the resource.
        ---
        Recommended practice is to include sufficient bibliographic detail to identify the resource
        as unambiguously as possible."""
        return self.terms["bibliographicCitation"] if "bibliographicCitation" in self.terms else None

    @bibliographicCitation.setter
    def bibliographicCitation(self, value):
        self.terms["bibliographicCitation"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def conformsTo(self):
        """An established standard to which the described resource conforms."""
        return self.terms["conformsTo"] if "conformsTo" in self.terms else None

    @conformsTo.setter
    def conformsTo(self, value):
        self.terms["conformsTo"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def contributor(self):
        """
        An entity responsible for making contributions to the resource.
        ---
        The guidelines for using names of persons or organizations as creators apply to contributors."""
        return self.terms["contributor"] if "contributor" in self.terms else None

    @contributor.setter
    def contributor(self, value):
        self.terms["contributor"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def coverage(self):
        """
        The spatial or temporal topic of the resource, spatial applicability of the resource, or jurisdiction
        under which the resource is relevant.
        ---
        Spatial topic and spatial applicability may be a named place or a location specified by its geographic
        coordinates. Temporal topic may be a named period, date, or date range. A jurisdiction may be a named
        administrative entity or a geographic place to which the resource applies. Recommended practice is to use a
        controlled vocabulary such as the Getty Thesaurus of Geographic Names.
        https://www.getty.edu/research/tools/vocabulary/tgn/index.html
        Where appropriate, named places or time periods may be used in preference to numeric identifiers such as sets of
        coordinates or date ranges. Because coverage is so broadly defined, it is preferable to use the more specific
        subproperties Temporal Coverage and Spatial Coverage."""
        return self.terms["coverage"] if "coverage" in self.terms else None

    @coverage.setter
    def coverage(self, value):
        self.terms["coverage"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateCreated(self):
        """
        Date of creation of the resource.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        """
        return self.terms["created"] if "created" in self.terms else None

    @dateCreated.setter
    def dateCreated(self, value):
        self.terms["created"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def creator(self):
        """
        An entity responsible for making the resource.
        ---
        Recommended practice is to identify the creator with a URI. If this is not possible or feasible, a literal value
        that identifies the creator may be provided."""
        return self.terms["creator"] if "creator" in self.terms else None

    @creator.setter
    def creator(self, value):
        self.terms["creator"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def date(self):
        """
        A point or period of time associated with an event in the lifecycle of the resource.
        ---
        Date may be used to express temporal information at any level of granularity. Recommended practice is to express
        the date, date/time, or period of time according to ISO 8601-1
        https://www.iso.org/iso-8601-date-and-time-format.html
        or a published profile of the ISO standard, such as the W3C Note on Date and Time Formats
        https://www.w3.org/TR/NOTE-datetime
        or the Extended Date/Time Format Specification.
        http://www.loc.gov/standards/datetime/
        If the full date is unknown, month and year (YYYY-MM) or just year (YYYY) may be used. Date ranges may be
        specified using ISO 8601 period of time specification in which start and end dates are separated by
        a '/' (slash) character. Either the start or end date may be missing."""
        return self.terms["date"] if "date" in self.terms else None

    @date.setter
    def date(self, value):
        self.terms["date"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateAccepted(self):
        """
        Date of acceptance of the resource.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        Examples of resources to which a date of acceptance may be relevant are a thesis
        (accepted by a university department) or an article (accepted by a journal)."""
        return self.terms["accepted"] if "accepted" in self.terms else None

    @dateAccepted.setter
    def dateAccepted(self, value):
        self.terms["accepted"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateCopyrighted(self):
        """
        Date of copyright of the resource.
        ---
        Typically a year. Recommended practice is to describe the date, date/time,
        or period of time as recommended for the property Date."""
        return self.terms["copyrighted"] if "copyrighted" in self.terms else None

    @dateCopyrighted.setter
    def dateCopyrighted(self, value):
        self.terms["copyrighted"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateSubmitted(self):
        """
        Date of submission of the resource.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        Examples of resources to which a 'Date Submitted' may be relevant include a thesis
        (submitted to a university department) or an article (submitted to a journal)."""
        return self.terms["submitted"] if "submitted" in self.terms else None

    @dateSubmitted.setter
    def dateSubmitted(self, value):
        self.terms["submitted"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def description(self):
        """
        An account of the resource.
        ---
        Description may include but is not limited to: an abstract, a table of contents, a graphical representation,
        or a free-text account of the resource."""
        return self.terms["description"] if "description" in self.terms else None

    @description.setter
    def description(self, value):
        self.terms["description"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def extent(self):
        """
        The size or duration of the resource.
        ---
        Recommended practice is to specify the file size in megabytes and duration in ISO 8601 format."""
        return self.terms["extent"] if "extent" in self.terms else None

    @extent.setter
    def extent(self, value):
        self.terms["extent"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def format(self):
        """
        The file format, physical medium, or dimensions of the resource.
        ---
        Recommended practice is to use a controlled vocabulary where available. For example, for file formats one could
        use the list of Internet Media Types [MIME]. Examples of dimensions include size and duration."""
        return self.terms["format"] if "format" in self.terms else None

    @format.setter
    def format(self, value):
        self.terms["format"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationHasFormat(self):
        """
        A related resource that is substantially the same as the pre-existing described resource, but in another format.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of
        Is Format Of.
        The data is stored as a list. Use addRelationHasFormat to append data or extend the list"""
        return self.terms["hasFormat"] if "hasFormat" in self.terms else None

    @relationHasFormat.setter
    def relationHasFormat(self, value):
        self._set_term_ensure_list("hasFormat", value)

    def addRelationHasFormat(self, value):
        """Add a relation of type hasFormat."""
        self.add_to_term("hasFormat", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationHasPart(self):
        """
        A related resource that is included either physically or logically in the described resource.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property
        of Is Part Of.
        The data is stored as a list. Use addRelationHasPart to append data or extend the list"""
        return self.terms["hasPart"] if "hasPart" in self.terms else None

    @relationHasPart.setter
    def relationHasPart(self, value):
        self._set_term_ensure_list("hasPart", value)

    def addRelationHasPart(self, value):
        """Add a relation of type hasPart."""
        self.add_to_term("hasPart", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationHasVersion(self):
        """
        A related resource that is a version, edition, or adaptation of the described resource.
        ---
        Changes in version imply substantive changes in content rather than differences in format. This property is 
        intended to be used with non-literal values. This property is an inverse property of Is Version Of.
        The data is stored as a list. Use addRelationHasVersion to append data or extend the list"""
        return self.terms["hasVersion"] if "hasVersion" in self.terms else None

    @relationHasVersion.setter
    def relationHasVersion(self, value):
        self._set_term_ensure_list("hasVersion", value)

    def addRelationHasVersion(self, value):
        """Add a relation of type hasVersion."""
        self.add_to_term("hasVersion", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def identifier(self):
        """
        An unambiguous reference to the resource within a given context.
        ---
        Recommended practice is to identify the resource by means of a string conforming to an identification system.
        Examples include International Standard Book Number (ISBN), Digital Object Identifier (DOI), and Uniform
        Resource Name (URN). Persistent identifiers should be provided as HTTP URIs."""
        return self.terms["identifier"] if "identifier" in self.terms else None

    @identifier.setter
    def identifier(self, value):
        self.terms["identifier"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def instructionalMethod(self):
        """
        A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.
        ---
        Instructional Method typically includes ways of presenting instructional materials or conducting instructional
        activities, patterns of learner-to-learner and learner-to-instructor interactions, and mechanisms by which group
        and individual levels of learning are measured. Instructional methods include all aspects of the instruction and
        learning processes from planning and implementation through evaluation and feedback."""
        return self.terms["instructionalMethod"] if "instructionalMethod" in self.terms else None

    @instructionalMethod.setter
    def instructionalMethod(self, value):
        self.terms["instructionalMethod"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsFormatOf(self):
        """
        A pre-existing related resource that is substantially the same as the described resource, but in another format.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of
        Has Format. Use addRelationIsFormatOf to append data or extend the list."""
        return self.terms["isFormatOf"] if "isFormatOf" in self.terms else None

    @relationIsFormatOf.setter
    def relationIsFormatOf(self, value):
        self._set_term_ensure_list("isFormatOf", value)

    def addRelationIsFormatOf(self, value):
        """Add a relation of type isFormatOf."""
        self.add_to_term("isFormatOf", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsPartOf(self):
        """
        A related resource in which the described resource is physically or logically included.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of Has Part.
        Use addRelationIsPartOf to append data or extend the list.
        """
        return self.terms["isPartOf"] if "isPartOf" in self.terms else None

    @relationIsPartOf.setter
    def relationIsPartOf(self, value):
        self._set_term_ensure_list("isPartOf", value)

    def addRelationIsPartOf(self, value):
        """Add a relation of type isPartOf."""
        self.add_to_term("isPartOf", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsReferencedBy(self):
        """
        A related resource in which the described resource is physically or logically included.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of Has Part.
        Use addRelationIsReferencedBy to append data or extend the list.
        """
        return self.terms["isReferencedBy"] if "isReferencedBy" in self.terms else None

    @relationIsReferencedBy.setter
    def relationIsReferencedBy(self, value):
        self._set_term_ensure_list("isReferencedBy", value)

    def addRelationIsReferencedBy(self, value):
        """Add a relation of type isReferencedBy."""
        self.add_to_term("isReferencedBy", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsReplacedBy(self):
        """
        A related resource that supplants, displaces, or supersedes the described resource.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of Replaces.
        Use addRelationIsReplacedBy to append data or extend the list.
        """
        return self.terms["isReplacedBy"] if "isReplacedBy" in self.terms else None

    @relationIsReplacedBy.setter
    def relationIsReplacedBy(self, value):
        self._set_term_ensure_list("isReplacedBy", value)

    def addRelationIsReplacedBy(self, value):
        """Add a relation of type isReplacedBy."""
        self.add_to_term("isReplacedBy", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsRequiredBy(self):
        """
        A related resource that requires the described resource to support its function, delivery, or coherence.
        ---
        This property is intended to be used with non-literal values. This property is an inverse property of Requires.
        Use addRelationIsRequiredBy to append data or extend the list.
        """
        return self.terms["isRequiredBy"] if "isRequiredBy" in self.terms else None

    @relationIsRequiredBy.setter
    def relationIsRequiredBy(self, value):
        self._set_term_ensure_list("isRequiredBy", value)

    def addRelationIsRequiredBy(self, value):
        """Add a relation of type isRequiredBy."""
        self.add_to_term("isRequiredBy", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateIssued(self):
        """
        Date of formal issuance of the resource.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        """
        return self.terms["issued"] if "issued" in self.terms else None

    @dateIssued.setter
    def dateIssued(self, value):
        self.terms["issued"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationIsVersionOf(self):
        """
        A related resource of which the described resource is a version, edition, or adaptation.
        ---
        Changes in version imply substantive changes in content rather than differences in format. This property is
        intended to be used with non-literal values. This property is an inverse property of Has Version.
        Use addRelationIsVersionOf to append data or extend the list.
        """
        return self.terms["isVersionOf"] if "isVersionOf" in self.terms else None

    @relationIsVersionOf.setter
    def relationIsVersionOf(self, value):
        self._set_term_ensure_list("isVersionOf", value)

    def addRelationIsVersionOf(self, value):
        """Add a relation of type isVersionOf."""
        self.add_to_term("isVersionOf", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def language(self):
        """
        A language of the resource.
        ---
        Recommended practice is to use either a non-literal value representing a language from a controlled vocabulary
        such as ISO 639-2 or ISO 639-3, or a literal value consisting of an IETF Best Current Practice 47
        https://tools.ietf.org/html/bcp47
        language tag.
        """
        return self.terms["language"] if "language" in self.terms else None

    @language.setter
    def language(self, value):
        self.terms["language"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def license(self):
        """
        A legal document giving official permission to do something with the resource.
        ---
        Recommended practice is to identify the license document with a URI. If this is not possible or feasible, a
        literal value that identifies the license may be provided.
        """
        return self.terms["license"] if "license" in self.terms else None

    @license.setter
    def license(self, value):
        self.terms["license"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def mediator(self):
        """
        An entity that mediates access to the resource.
        ---
        In an educational context, a mediator might be a parent, teacher, teaching assistant, or care-giver.
        """
        return self.terms["mediator"] if "mediator" in self.terms else None

    @mediator.setter
    def mediator(self, value):
        self.terms["mediator"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def medium(self):
        """
        The material or physical carrier of the resource.
        """
        return self.terms["medium"] if "medium" in self.terms else None

    @medium.setter
    def medium(self, value):
        self.terms["medium"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateModified(self):
        """
        Date on which the resource was changed.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        """
        return self.terms["modified"] if "modified" in self.terms else None

    @dateModified.setter
    def dateModified(self, value):
        self.terms["modified"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def provenance(self):
        """
        A statement of any changes in ownership and custody of the resource since its creation that are significant for
        its authenticity, integrity, and interpretation.
        ---
        The statement may include a description of any changes successive custodians made to the resource.
        """
        return self.terms["provenance"] if "provenance" in self.terms else None

    @provenance.setter
    def provenance(self, value):
        self.terms["provenance"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def publisher(self):
        """
        An entity responsible for making the resource available.
        """
        return self.terms["publisher"] if "publisher" in self.terms else None

    @publisher.setter
    def publisher(self, value):
        self.terms["publisher"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationReferences(self):
        """
        A related resource that is referenced, cited, or otherwise pointed to by the described resource.
        ---
        This property is intended to be used with non-literal values.
        This property is an inverse property of Is Referenced By.
        Use addRelationReferences to append data or extend the list.
        """
        return self.terms["references"] if "references" in self.terms else None

    @relationReferences.setter
    def relationReferences(self, value):
        self._set_term_ensure_list("references", value)

    def addRelationReferences(self, value):
        """Add a relation of type references."""
        self.add_to_term("references", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relation(self):
        """
        A related resource.
        ---
        Recommended practice is to identify the related resource by means of a URI. If this is not possible or feasible,
        a string conforming to a formal identification system may be provided.
        Use addRelation to append data or extend the list.
        """
        return self.terms["relation"] if "relation" in self.terms else None

    @relation.setter
    def relation(self, value):
        self._set_term_ensure_list("relation", value)

    def addRelation(self, value):
        """Add a general relation."""
        self.add_to_term("relation", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationReplaces(self):
        """
        A related resource that is supplanted, displaced, or superseded by the described resource.
        ---
        This property is intended to be used with non-literal values.
        This property is an inverse property of Is Replaced By.
        Use addRelationReplaces to append data or extend the list.
        """
        return self.terms["replaces"] if "replaces" in self.terms else None

    @relationReplaces.setter
    def relationReplaces(self, value):
        self._set_term_ensure_list("replaces", value)

    def addRelationReplaces(self, value):
        """Add a relation of type replaces."""
        self.add_to_term("replaces", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationRequires(self):
        """
        A related resource that is required by the described resource to support its function, delivery, or coherence.
        ---
        This property is intended to be used with non-literal values.
        This property is an inverse property of Is Required By.
        Use addRelationRequires to append data or extend the list.
        """
        return self.terms["requires"] if "requires" in self.terms else None

    @relationRequires.setter
    def relationRequires(self, value):
        self._set_term_ensure_list("requires", value)

    def addRelationRequires(self, value):
        """Add a relation of type requires."""
        self.add_to_term("requires", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def rights(self):
        """
        Information about rights held in and over the resource.
        ---
        Typically, rights information includes a statement about various property rights associated with the resource,
        including intellectual property rights. Recommended practice is to refer to a rights statement with a URI.
        If this is not possible or feasible, a literal value (name, label, or short text) may be provided.
        """
        return self.terms["rights"] if "rights" in self.terms else None

    @rights.setter
    def rights(self, value):
        self.terms["rights"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def rightsHolder(self):
        """
        A person or organization owning or managing rights over the resource.
        ---
        Recommended practice is to refer to the rights holder with a URI. If this is not possible or feasible, a literal
        value that identifies the rights holder may be provided.
        """
        return self.terms["rightsHolder"] if "rightsHolder" in self.terms else None

    @rightsHolder.setter
    def rightsHolder(self, value):
        self.terms["rightsHolder"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def relationSource(self):
        """
        A related resource from which the described resource is derived.
        ---
        This property is intended to be used with non-literal values. The described resource may be derived from the
        related resource in whole or in part. Best practice is to identify the related resource by means of a URI or
        a string conforming to a formal identification system.
        Use addRelationSource to append data or extend the list.
        """
        return self.terms["source"] if "source" in self.terms else None

    @relationSource.setter
    def relationSource(self, value):
        self._set_term_ensure_list("source", value)

    def addRelationSource(self, value):
        """Add a relation of type Source."""
        self.add_to_term("source", value)
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def spatial(self):
        """
        Spatial characteristics of the resource.
        """
        return self.terms["spatial"] if "spatial" in self.terms else None

    @spatial.setter
    def spatial(self, value):
        self.terms["spatial"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def subject(self):
        """
        A topic of the resource.
        ---
        Recommended practice is to refer to the subject with a URI. If this is not possible or feasible, a literal
        value that identifies the subject may be provided. Both should preferably refer to a subject in
        a controlled vocabulary.
        """
        return self.terms["subject"] if "subject" in self.terms else None

    @subject.setter
    def subject(self, value):
        self.terms["subject"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def temporal(self):
        """
        Temporal characteristics of the resource.
        """
        return self.terms["temporal"] if "temporal" in self.terms else None

    @temporal.setter
    def temporal(self, value):
        self.terms["temporal"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def title(self):
        """
        A name given to the resource.
        """
        return self.terms["title"] if "title" in self.terms else None

    @title.setter
    def title(self, value):
        self.terms["title"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def type(self):
        """
        The nature or genre of the resource.
        ---
        Recommended practice is to use a controlled vocabulary such as the DCMI Type Vocabulary.
        http://dublincore.org/documents/dcmi-type-vocabulary/
        To describe the file format, physical medium, or dimensions of the resource, use the property Format.
        """
        return self.terms["type"] if "type" in self.terms else None

    @type.setter
    def type(self, value):
        self.terms["type"] = value
    # ------------------------------------------------------------------------------------------------------------------

    @property
    def dateValid(self):
        """
        Date (often a range) of validity of a resource.
        ---
        Recommended practice is to describe the date, date/time, or period of time as recommended for the property Date.
        """
        return self.terms["valid"] if "valid" in self.terms else None

    @dateValid.setter
    def dateValid(self, value):
        self.terms["valid"] = value
    # ------------------------------------------------------------------------------------------------------------------
