
#
#  This Python package `pyDuCo` and its source code are licensed under the CC BY 4.0 license
#
#  "Dublin Core" is a protected under common law trademark of the Dublin Core™ Metadata Initiative (DCMI) with global
#  recognition. The Dublin Core™ metadata specification is licensed under the CC BY 4.0 license. The usage of the
#  specification in/with this software is subject to this license.
#
#  https://creativecommons.org/licenses/by/4.0/
#  https://www.dublincore.org/about/copyright/
#  https://www.dublincore.org/specifications/dublin-core/dcmi-terms/
#


import yaml
from dublincore.metadata import MetaData
import os


def metadata2yaml(metadata, path):
    """
    Writes given MetaData object into a file, specified by path. An existing file will be overwritten.
        argument 1: MetaData object
        argument 2: file path
    """

    # check whether the specified path exists
    basedir = os.path.dirname(path)
    if (not basedir == '') and (not os.path.isdir(basedir)):
        raise FileNotFoundError("Directory " + basedir + " not found")

    # open write and close the file
    text = yaml.dump(metadata.terms)
    file = open(path, 'w')
    try:
        file.writelines(text)
    finally:
        file.close()


def yaml2metadata(path) -> MetaData:
    """
    Loads a MetaData object from a yaml file, specified by path. Raises an error if the file is not found
        argument 1: file path
           returns: MetaData object
    """

    if not os.path.isfile(path):
        raise FileNotFoundError("File " + path + " not found")

    file = open(path, 'r')
    try:
        text = file.readlines()
    finally:
        file.close()

    my_meta = MetaData()
    my_meta.terms = yaml.safe_load("\n".join(text))

    return my_meta
